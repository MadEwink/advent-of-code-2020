## About this project and myself

The Advent Of Code is a programming challenge, find information there : [Advent Of Code site](https://adventofcode.com/)
I am not participating for the competition, I just wanted to find a challenge to improve my skills at ADA programming.
Speaking of my skills, I am a beginner : I began coding in ADA in 2017, using an old ADA book my father gave me. Ever since, I discovered, there was recent versions of ADA, like 2012, and for now I have **not** read a recent ADA course. I plan to, but it is not part of my studies, which take me a lot of time.
Therefore, as a self taught ADA beginner programmer, I don't expect my code to be any reference. I just am toying with stuff and trying to figure out how stuff should be done. I thought it would be interesting to write my feedback on my own code somewhere, so I began to write this readme.

## How I feel about what I wrote

### Day 1
I think this first day was quite simple, and using the knowledge I acquired dealing with the first day of the 2018 edition (the only day I did then), I think it went out not so bad.

### Day 2
At first, I thought it would go easy and quickly as day 1. I was wrong. I think the challenge was quite an easy one too, but I faced a lot of problems I was not expecting, because I am not so familiar with ADA. I think there is a better way of doing things than just creating a dumb entry point as I did, but I did not spend much time on that.
What pisses me the most is the way I create many files, and spend too much time opening, closing, reading and writing in files. This would have been way easier using lists, and I guess I will have to learn more about it. For part 1 it was no problem, but fot part 2, computing the result takes minutes (did not measure though). Since it seems that this IntCode interpreter will be reused, I hope it will not be a problem, since I doubt I will have time to rewrite it. And if the calculus are too long, I'll need to manualy adjust the program so that it can do more than 999 operations. That is so dumb and dirty...

### Day 3
This time, I used arrays, and I guess it is for the better. However, this does not mean I did something great. I had two ideas, and thought both were not that good, but for some reason I did not sleep tonight, and I think I am too tired to see if things could be done in a way more clever way...
The first idea I had involved storing a list of all the positions a wire goes through, and then finding all the intersections. This seemed quite brutal to me, and I had no idea how to do it in a clean and proper way in ADA, so I went for the other solution...
Which was just as brutal as the first one, but I had an idea of how I could implement it. It consists in going through all the positions that the first wire goes through, and for each position, see if the second wire goes through it too. It takes quite a long time. Infact, I am writing this, hoping it will work well, waiting for it to end... It has found at least one intersection for now (if it finds intersections that are farther, it will not notice me).
Even though I used arrays, I am still disappointed in myself : I could not figure how to use unbounded arrays, so I don't see how I could get my arrays out of the procedure that creates them. Maybe if I only had one to return, a function would do the trick, but I'm not sure. So instead of dealing with this problem, I just put all the code I would have loved to separate into one big dumb procedure...
(Yay ! It just terminated, and it was the correct answer !)
For the second part, I still had this problem of arrays, so I created a large array I hoped would be big enough (and it proved to be so). Let alone that, I think I managed to adapt my code (which was awful) by improving it a little, or at least not making it worse.

### Day 4
I think this day was quite simple, compared with the two last ones. I just created a simple procedure checking all the properties. When iterating over the password range, I tried to create a range type, but it did not turn well, so I just used two integers, and I think it could be done better.

### Day 5
As expected from day 2, the IntCode returned. I took my old code, and first redesigned the whole thing in order to use lists, and I found a way of doing it ! I don't think it is perfect, but I am glad I finally managed to do it. It is now much faster, so I did not loose time on waiting for the result. However, I just discovered the package Collections, which would have helped a lot I guess. Since I am writing this on day 6, even though I can not begin it now, I think this package will help me a lot !

### Day 6
And thank goodness, this package is so helpful ! I could in just a few dozens of minutes implement a tree which allowed me to do all the computations quite easily. I guess once again it's not perfect, but I am satisfied, since I know I made some progress again. I also had a try using something like Iterate_Children, which takes a procedure as argument, and even if I did not keep it, I learned some more things about access types.

### Day 7
The first part was quite easy, but I made stupid mistakes, and I guess writing manually five loops was not the best idea. I then considered doing the second part and it seems much more complicated. I have two ideas for solving this :
* Using threads (or Tasks, since it seems to be the Ada equivalent of threads), with a looping producer-consumer scheme
* Using named pipes, and creating five programs interacting through them, and after a small research, it seems Ada makes it possible using streams
Those two solutions require learning a complete new skill in Ada (Tasks or Streams), and it seems to be quite complicated stuff. I do not think I have the time for learning those by myself, and I would gladly prefer having a real course. It is thus with sadness that I decide (for now) not to deal with the second challenge.

### Day 8
It seemed to be quite an easy day, but was a great opportunity to practive the Containers package once again. I still managed to do stupid errors on array bounds, which cost me some time, but I did not encounter any major problem.

## Making a separation here become time has passed

More than four month have passed since I last wrote some code. I did not update the readme, but I did some stuff for day 9 and 10.

### Day 9
I do not remember the difficulties I encountered, but I think I remember it was not trivial to me. I had some time to figure out how to fix my mistakes, but eventually did it.

### Day 10
I have a hard time doing the second part, and I did not manage to do it properly. I remember having monitored a bit a saw that there was a problem with the way I detect asteroids while rotating.

### Day 11
It took some time to get back to it, and a small test revealed a problem in my intcode module, making me pause this project for now. I may try to debug my stuff or to rewrite it someday.
