with ADA.TEXT_IO; use ADA.TEXT_IO;

procedure ModuleMass is
	Line : String(1..10);
	LineLength : Integer;
	PartMass : Integer;
	PartFuel : Integer;
	TotalFuel : Integer := 0;
	File : File_Type;
begin
	Open(File, IN_FILE, "input");
	while not END_OF_FILE(File) loop
		GET_LINE(File,Line,LineLength);
		PartMass := Integer'Value(Line(Line'First..LineLength));
		PartFuel := (PartMass/3)-2;
		TotalFuel := TotalFuel + PartFuel;
		while (PartFuel/3-2) > 0 loop
			PartFuel := (PartFuel/3)-2;
			TotalFuel := TotalFuel + PartFuel;
		end loop;
	end loop;
	Close(File);
	Put_Line("Total Fuel needed : " & TotalFuel'Image);
end ModuleMass;
