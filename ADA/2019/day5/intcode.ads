with ADA.Text_IO; use ADA.Text_IO;

package Intcode is
	type Vector is array (Integer range <>) of Integer;
	function GetCodeLength return Integer;
	function GetIntCode(CodeLength : Integer) return Vector;
	procedure ExecuteInstruction(IntCodeVector : IN OUT Vector; InstructionPosition : IN OUT Integer; Instruction : IN Integer; Value1, Value2, Address : IN Integer);
	procedure PositionInstruction(IntCodeVector : IN Vector; InstructionPosition : IN Integer; Instruction, Value1, Value2, Address : OUT Integer);
	procedure Interprete;
end Intcode;
