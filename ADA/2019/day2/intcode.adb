with ADA.Text_IO; use ADA.Text_IO;

package body Intcode is
	IntCodeFile : File_Type;
	IntCodeFileName : String(1..8) := "input000";
	IntCodeFileNameNumber : Integer := 0;
	IntCodePosition : Integer := 0;

	procedure GetIntAt(Position : IN Integer; Result : OUT Integer) is
		c : Character;
	begin
		if not Is_Open(IntCodeFile) then
			Open(IntCodeFile, IN_FILE, IntCodeFileName);
			IntCodePosition := 0;
		end if;
		if (IntCodePosition > Position) then
			Close(IntCodeFile);
			Open(IntCodeFile, IN_FILE, IntCodeFileName);
			IntCodePosition := 0;
		end if;
		-- position ourself at value
		while (IntCodePosition < Position) loop
			loop
				Get(IntCodeFile, c);
				exit when (c = ',');
			end loop;
			IntCodePosition := IntCodePosition + 1;
		end loop;
		-- read value
		Result := 0;
		loop
			Get(IntCodeFile,c);
			exit when (c = ',');
			Result := Result * 10;
			case c is
				when '1' => Result := Result + 1;
				when '2' => Result := Result + 2;
				when '3' => Result := Result + 3;
				when '4' => Result := Result + 4;
				when '5' => Result := Result + 5;
				when '6' => Result := Result + 6;
				when '7' => Result := Result + 7;
				when '8' => Result := Result + 8;
				when '9' => Result := Result + 9;
				when others => null;
			end case;
			exit when End_Of_File(IntCodeFile);
		end loop;
		IntCodePosition := IntCodePosition + 1;
	end GetIntAt;

	procedure UpdateIntCode(ResultPosition, Result : Integer) is
		NewFile : File_Type;
		NewFileName : String(1..8);
		c : Character;
	begin
		if Is_Open(IntCodeFile) then
			Close(IntCodeFile);
		end if;
		Open(IntCodeFile, IN_FILE, IntCodeFileName);
		IntCodePosition := 0;
		IntCodeFileNameNumber := IntCodeFileNameNumber + 1;
		if (IntCodeFileNameNumber < 10) then
			NewFileName := IntCodeFileName(1..5) & "00" & String (IntCodeFileNameNumber'Image)(2);
		elsif (IntCodeFileNameNumber < 100) then
			NewFileName := IntCodeFileName(1..5) & "0" & String (IntCodeFileNameNumber'Image)(2..3);
		else
			NewFileName := IntCodeFileName(1..5) & String (IntCodeFileNameNumber'Image)(2..4);
		end if;
		Create(NewFile, OUT_FILE, NewFileName);
		-- copy file beginning
		while IntCodePosition < ResultPosition loop
			Get(IntCodeFile, c);
			Put(NewFile, c);
			if (c = ',') then
				IntCodePosition := IntCodePosition +1;
			end if;
		end loop;
		-- write new result
		for c of Result'Image loop
			if c in '0'..'9' then
				Put(NewFile, c);
			end if;
		end loop;
		Put(NewFile, ',');
		-- pass replaced number
		loop
			Get(IntCodeFile, c);
			exit when (c=',' or End_Of_File(IntCodeFile));
		end loop;
		IntCodePosition := IntCodePosition + 1;
		-- copy file end
		while not End_Of_File(IntCodeFile) loop
			Get(IntCodeFile, c);
			Put(NewFile, c);
			if (c = ',') then
				IntCodePosition := IntCodePosition +1;
			end if;
		end loop;
		Close(IntCodeFile);
		Close(NewFile);
		IntCodeFileName := NewFileName;
	end UpdateIntCode;

	procedure Interprete(Noun, Verb : IN Integer; Result : OUT Integer) is
		CurrentInstructionPosition : Integer := 0;
		CurrentInstruction : Integer := 0;
		FirstValuePosition : Integer;
		FirstValue : Integer;
		SecondValuePosition : Integer;
		SecondValue : Integer;
		CalculusResultPosition : Integer;
		CalculusResult : Integer;
	begin
		-- initialize as instructed
		IntCodeFileName := "input000";
		IntCodeFileNameNumber := 0;
		UpdateIntCode(1,Noun);
		UpdateIntCode(2,Verb);
		loop
			GetIntAt(CurrentInstructionPosition, CurrentInstruction);
			exit when CurrentInstruction = 99;
			-- CurrentInstruction is 1 or 2
			GetIntAt(CurrentInstructionPosition + 1, FirstValuePosition);
			GetIntAt(CurrentInstructionPosition + 2, SecondValuePosition);
			GetIntAt(CurrentInstructionPosition + 3, CalculusResultPosition);
			GetIntAt(FirstValuePosition, FirstValue);
			GetIntAt(SecondValuePosition, SecondValue);
			case CurrentInstruction is
				when 1 => CalculusResult := FirstValue+SecondValue;
				when 2 => CalculusResult := FirstValue*SecondValue;
				when others => null;
			end case;
			UpdateIntCode(CalculusResultPosition, CalculusResult);
			CurrentInstructionPosition := CurrentInstructionPosition + 4;
		end loop;
		GetIntAt(0, Result);
		Close(IntCodeFile);
	end Interprete;
end Intcode;

