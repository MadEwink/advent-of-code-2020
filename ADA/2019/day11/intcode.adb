with Ada.Text_IO; use Ada.Text_IO;
with Ada.Long_Integer_Text_IO; use Ada.Long_Integer_Text_IO;

package body Intcode is
	use IntcodeVectors;
	IntCodeFileName : String := "input";

	procedure PrintIntCode(IntCodeVar : IN IntCode_Type) is
	begin
		Put("Vector (" & IntCodeVar.currentInstructionPosition'Image & '/' & Length(IntCodeVar.intcode)'Image & ',' & IntCodeVar.relativeBase'Image & " ) : ");
		for Index in First_Index(IntCodeVar.intcode)..Last_Index(IntCodeVar.intcode) loop
			if Index = IntCodeVar.CurrentInstructionPosition then Put("<"); end if;
			Put(Element(IntCodeVar.intcode, Index)'Image);
			if Index = IntCodeVar.CurrentInstructionPosition then Put(">"); end if;
			Put(",");
		end loop;
		Put_Line(" ");
	end;

	function GetIntCode return IntCode_Type is
		IntCodeVar : IntCode_Type := (
			intcode => Empty_Vector,
			currentInstructionPosition => 0,
			relativeBase => 0
			);
		File : File_Type;
		Element : Long_Integer;
		c : Character;
	begin
		Open(File, IN_FILE, IntCodeFileName);
		loop
			Get(File, Element);
			IntCodeVar.intcode.Append(Element);
			exit when End_Of_File(File);
			Get(File,c);
		end loop;
		Close(File);
		return IntCodeVar;
	end GetIntCode;

	procedure AdaptVectorLength(VectorVar : IN OUT Vector; Address : Natural) is
	begin
		while Address >= Natural(Length(VectorVar)) loop
			VectorVar.Append(0);
		end loop;
	end AdaptVectorLength;

	procedure ExecuteInstruction(IntCodeVar : IN OUT IntCode_Type; Instruction : IN Long_Integer; Value1, Value2 : IN Long_Integer; Address : Natural) is
		Value : Long_Integer;
	begin
		case Instruction is
			when 1 =>
				AdaptVectorLength(IntCodeVar.intcode, Address);
				Replace_Element(IntCodeVar.intcode,Address,Value1+Value2);
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 4;
			when 2 =>
				AdaptVectorLength(IntCodeVar.intcode, Address);
				Replace_Element(IntCodeVar.intcode,Address,Value1*Value2);
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 4;
			when 3 =>
				AdaptVectorLength(IntCodeVar.intcode, Address);
				--Put(Standard_Output,"Enter input : ");
				Get(Value);
				Replace_Element(IntCodeVar.intcode,Address,Value);
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 2;
			when 4 =>
				Value := Element(IntCodeVar.intcode,Address);
				Put_Line(Long_Integer'Image(Value));
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 2;
			when 5 =>
				if Value1 /= 0 then
					IntCodeVar.currentInstructionPosition := Natural(Value2);
				else
					IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 3;
				end if;
			when 6 =>
				if Value1 = 0 then
					IntCodeVar.currentInstructionPosition := Natural(Value2);
				else
					IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 3;
				end if;
			when 7 =>
				AdaptVectorLength(IntCodeVar.intcode, Address);
				if Value1 < Value2 then
					Replace_Element(IntCodeVar.intcode,Address,1);
				else
					Replace_Element(IntCodeVar.intcode,Address,0);
				end if;
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 4;
			when 8 =>
				AdaptVectorLength(IntCodeVar.intcode, Address);
				if Value1 = Value2 then
					Replace_Element(IntCodeVar.intcode,Address,1);
				else
					Replace_Element(IntCodeVar.intcode,Address,0);
				end if;
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition + 4;
			when 9 =>
				IntCodeVar.relativeBase := Natural(Long_Integer(IntCodeVar.relativeBase)+Value1);
				IntCodeVar.currentInstructionPosition := IntCodeVar.currentInstructionPosition+2;
			when others => null;
		end case;
	end ExecuteInstruction;

	procedure PositionValue(IntCodeVar : IN OUT IntCode_Type; Mode : IN Long_Integer; Offset : IN Natural; Value : OUT Long_Integer) is
		AddressFinal : Natural;
	begin
		if Mode = 0 then
			AddressFinal := Natural(Element(IntCodeVar.intcode,IntCodeVar.currentInstructionPosition+Offset));
			AdaptVectorLength(IntCodeVar.intcode, AddressFinal);
			Value := Element(IntCodeVar.intcode,AddressFinal);
		elsif Mode = 1 then
			Value := Element(IntCodeVar.intcode,IntCodeVar.currentInstructionPosition+Offset);
		else
			AddressFinal := Natural(Long_Integer(IntCodeVar.relativeBase)+Element(IntCodeVar.intcode, IntCodeVar.currentInstructionPosition+Offset));
			AdaptVectorLength(IntCodeVar.intcode, AddressFinal);
			Value := Element(IntCodeVar.intcode, AddressFinal);
		end if;
	end PositionValue;

	procedure PositionAddress(IntCodeVar : IN OUT IntCode_Type; Mode : IN Long_Integer; Offset : IN Natural; Address : OUT Natural; CanBeModeZero : Boolean := True) is
		AddressFinal : Natural;
	begin
		if Mode = 0 and CanBeModeZero then
			Address := Natural(Element(IntCodeVar.intcode,Natural(Element(IntCodeVar.intcode,IntCodeVar.currentInstructionPosition+Offset))));
		elsif Mode = 1 or (Mode = 0 and not CanBeModeZero) then
			Address := Natural(Element(IntCodeVar.intcode,IntCodeVar.currentInstructionPosition+Offset));
		else
			AddressFinal := Natural(Long_Integer(IntCodeVar.relativeBase)+Element(IntCodeVar.intcode, IntCodeVar.currentInstructionPosition+Offset));
			AdaptVectorLength(IntCodeVar.intcode, AddressFinal);
			Address := AddressFinal;
		end if;
	end PositionAddress;

	procedure PositionInstruction(IntCodeVar : IN OUT IntCode_Type; Instruction, Value1, Value2 : OUT Long_Integer; Address : OUT Natural) is
		Modes : array (Long_Integer range 1..3) of Long_Integer := (others => 0);
	begin
		Instruction := Element(IntCodeVar.intcode,IntCodeVar.currentInstructionPosition);
		if Instruction > 99 then
			Modes(1) := (Instruction/100) - 10*(Instruction/1000);
			Modes(2) := (Instruction/1000) - 10*(Instruction/10000);
			Modes(3) := (Instruction/10000) - 10*(Instruction/100000);
		end if;
		Instruction := Instruction - 100*(Instruction/100);
		case Instruction is
			when 1 | 2 =>
				PositionValue(IntCodeVar, Modes(1), 1, Value1);
				PositionValue(IntCodeVar, Modes(2), 2, Value2);
				PositionAddress(IntCodeVar, Modes(3), 3, Address, False);
			when 3 | 4 =>
				Value1 := 0;
				Value2 := 0;
				PositionAddress(IntCodeVar, Modes(1), 1, Address, False);
			when 5 | 6 =>
				PositionValue(IntCodeVar, Modes(1), 1, Value1);
				PositionValue(IntCodeVar, Modes(2), 2, Value2);
				Address := 0;
			when 7 | 8 =>
				PositionValue(IntCodeVar, Modes(1), 1, Value1);
				PositionValue(IntCodeVar, Modes(2), 2, Value2);
				PositionAddress(IntCodeVar, Modes(3), 3, Address, False);
			when 9 =>
				PositionValue(IntCodeVar, Modes(1), 1, Value1);
				Value2 := 0;
				Address := 0;
			when others =>
				Value1 := 0;
				Value2 := 0;
				Address := 0;
		end case;
	end PositionInstruction;

	procedure Interprete is
		CurrentInstruction : Long_Integer;
		Value1,Value2 : Long_Integer;
		Address : Natural;
		IntCodeVar : IntCode_Type := GetIntCode;
	begin
		loop
			PositionInstruction(IntCodeVar, CurrentInstruction, Value1, Value2, Address);
			exit when CurrentInstruction = 99;
			ExecuteInstruction(IntCodeVar, CurrentInstruction, Value1, Value2, Address);
		end loop;
	end Interprete;
end Intcode;

