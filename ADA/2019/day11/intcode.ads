with Ada.Containers.Vectors; use Ada.Containers;
with ADA.Text_IO; use ADA.Text_IO;

package Intcode is
	package IntcodeVectors is new Vectors(Natural, Long_Integer);
	use IntCodeVectors;
	type IntCode_Type is
		record
			intcode : Vector;
			currentInstructionPosition : Natural;
			relativeBase : Integer;
		end record;
	function GetIntCode return IntCode_Type;
	procedure ExecuteInstruction(IntCodeVar : IN OUT IntCode_Type; Instruction : IN Long_Integer; Value1, Value2 : IN Long_Integer; Address : Natural);
	procedure PositionInstruction(IntCodeVar : IN OUT IntCode_Type; Instruction, Value1, Value2 : OUT Long_Integer; Address : OUT Natural);
	procedure Interprete;
end Intcode;
