with Ada.Containers.Vectors; use Ada.Containers;

package Layers is
	LineNumber : Integer := 6;
	ColumnNumber : Integer := 25;
	type Layer_Type is array(1..LineNumber, 1..ColumnNumber) of Integer;
	package Vector_Image is new Vectors(Natural, Layer_Type); use Vector_Image;
	function ReadImage return Vector;
	function CountDigitOnLayer(Layer : Layer_Type; Digit : Integer) return Integer;
	function FindFewestZeroLayer(ImageData : Vector) return Layer_Type;
	procedure PrintLayer(Layer : Layer_Type);
	function ProduceFinalImage(ImageData : Vector) return Layer_Type;
end Layers;
