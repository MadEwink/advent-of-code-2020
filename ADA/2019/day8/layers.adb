with Ada.Text_IO; use Ada.Text_IO;

package body Layers is
	function ReadImage return Vector is
		ImageData : Vector;
		Layer : Layer_Type;
		Pixel : Integer;
		c : Character;
		File : File_Type;
		IndexLine, IndexColumn : Integer := 1;
	begin
		Open(File, IN_FILE, "input");
		while not End_Of_File(File) loop
			Get(File,c);
			Pixel := Integer'Value((1=>c));
			Layer(IndexLine, IndexColumn) := Pixel;
			IndexColumn := IndexColumn+1;
			if IndexColumn > ColumnNumber then
				IndexColumn := 1;
				IndexLine := IndexLine+1;
				if IndexLine > LineNumber then
					IndexLine := 1;
					ImageData.Append(Layer);
				end if;
			end if;
		end loop;
		Close(File);
		return ImageData;
	end ReadImage;

	function CountDigitOnLayer(Layer : Layer_Type; Digit : Integer) return Integer is
		Count : Integer := 0;
	begin
		for IndexLine in 1..LineNumber loop
			for IndexColumn in 1..ColumnNumber loop
				if (Layer(IndexLine, IndexColumn) = Digit) then
					Count := Count+1;
				end if;
			end loop;
		end loop;
		return Count;
	end CountDigitOnLayer;

	function FindFewestZeroLayer(ImageData : Vector) return Layer_Type is
		Layer : Layer_Type := ImageData(First_Index(ImageData));
		ZeroCount : Integer := CountDigitOnLayer(Layer, 0);
		CurrentCount : Integer;
	begin
		for Index in First_Index(ImageData)+1..Last_Index(ImageData) loop
			CurrentCount := CountDigitOnLayer(ImageData(Index), 0);
			if CurrentCount < ZeroCount then
				Layer := ImageData(Index);
				ZeroCount := CurrentCount;
			end if;
		end loop;
		return Layer;
	end FindFewestZeroLayer;

	procedure PrintLayer(Layer : Layer_Type) is
	begin
		for IndexLine in 1..LineNumber loop
			for IndexColumn in 1..ColumnNumber loop
				case Layer(IndexLine,IndexColumn) is
					when 0 => Put(" ");
					when 1 => Put("W");
					when others => Put("u");
				end case;
			end loop;
			Put_Line(" ");
		end loop;
	end PrintLayer;

	function ProduceFinalImage(ImageData : Vector) return Layer_Type is
		Layer : Layer_Type;
	begin
		for IndexLine in 1.. LineNumber loop
			for IndexColumn in 1..ColumnNumber loop
			declare
				Index : Integer := First_Index(ImageData);
				Pixel : Integer;
			begin
				loop
					Pixel := ImageData(Index)(IndexLine, IndexColumn);
					exit when Pixel /= 2 or Index = Last_Index(ImageData);
					Index := Index+1;
				end loop;
				Layer(IndexLine,IndexColumn) := Pixel;
			end;
			end loop;
		end loop;
		return Layer;
	end ProduceFinalImage;

end Layers;
