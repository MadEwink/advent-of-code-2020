with Ada.Text_IO; use Ada.Text_IO;
with Layers; use Layers;

procedure Main is
	ImageData : Vector_Image.Vector;
	FewestZeroLayer : Layer_Type;
begin
	ImageData := ReadImage;
	FewestZeroLayer := FindFewestZeroLayer(ImageData);
	Put_Line(Integer'Image(CountDigitOnLayer(FewestZeroLayer,1)*CountDigitOnLayer(FewestZeroLayer,2)));
	PrintLayer(ProduceFinalImage(ImageData));
end Main;

