with Ada.Containers.Multiway_Trees;
use Ada.Containers;

package Orbit is
	type Star is
		record
			name : String(1..3);
		end record;
	package OrbitTree is new Multiway_Trees (Star);
	procedure ReadInput;
	procedure ComputeOrbits;
	procedure DistanceToSanta;
end Orbit;

