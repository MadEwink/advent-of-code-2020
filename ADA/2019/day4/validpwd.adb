with Ada.Text_IO; use Ada.Text_IO;

package body ValidPwd is
	-- puzzle input
	RangeMin : Integer := 278384;
	RangeMax : Integer := 824795;

	function IsValid(Pwd : Integer) return Boolean is
		PassArray : array (0..5) of Integer;
		DigitCount : array(0..9) of Integer := (others => 0);
		TempInt : Integer;
		SameDigits : Boolean;
		Decrease : Boolean;
	begin
		-- is 6 digit
		if not (Pwd / 100000 /= 0 and Pwd / 1000000 = 0) then
			return False;
		end if;
		-- is within the range
		if not (RangeMin <= Pwd and Pwd <= RangeMax) then
			return False;
		end if;
		-- put the digits in the array
		TempInt := 0;
		for i in PassArray'Range loop
			PassArray(TempInt) := (Pwd/(10**TempInt) - (Pwd/(10**(TempInt+1)))*10);
			TempInt := TempInt+1;
		end loop;
		-- two adjacent digits
		-- never decrease
		TempInt := PassArray(PassArray'First);
		SameDigits := False;
		Decrease := False;
		DigitCount(PassArray(0)) := 1;
		for i in PassArray'First+1..PassArray'Last loop
			DigitCount(PassArray(i)) := DigitCount(PassArray(i))+1;
			if (TempInt < PassArray(i)) then
				Decrease := True;
			end if;
			TempInt := PassArray(i);
		end loop;
		for i in DigitCount'Range loop
			-- must be 2, no more, no less
			if DigitCount(i) = 2 then
				SameDigits := True;
			end if;
		end loop;
		return SameDigits and not Decrease;
	end IsValid;
end ValidPwd;
