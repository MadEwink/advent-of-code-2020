with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body CrossWire is
	FirstWirePathLength, SecondWirePathLength : Integer;
	procedure GetLengths is
		File : File_Type;
		c : Character;
	begin
		Open(File, IN_FILE, "input");
		FirstWirePathLength := 1;
		FIRST_PATH:
		loop
			Get(File, c);
			exit when End_Of_Line(File);
			if c = ',' then
				FirstWirePathLength := FirstWirePathLength+1;
			end if;
		end loop FIRST_PATH;
		SecondWirePathLength := 1;
		SECOND_PATH:
		loop
			Get(File, c);
			exit when End_Of_File(File);
			if c = ',' then
				SecondWirePathLength := SecondWirePathLength+1;
			end if;
		end loop SECOND_PATH;
		Close(File);
	end GetLengths;

	procedure ProgressThrough(Dir : IN Direction; Coordinate : IN OUT Coordinates) is
	begin
		case Dir is
			when 'U' => Coordinate(2) := Coordinate(2)+1;
			when 'D' => Coordinate(2) := Coordinate(2)-1;
			when 'R' => Coordinate(1) := Coordinate(1)+1;
			when 'L' => Coordinate(1) := Coordinate(1)-1;
		end case;
	end ProgressThrough;

	function GetManathanDistance(Coordinate : Coordinates) return Integer is
		Result : Integer;
	begin
		Result := abs Coordinate(1) + abs Coordinate(2);
		return Result;
	end GetManathanDistance;

	function Equals(C1, C2 : Coordinates) return Boolean is
	begin
		return (C1(1) = C2(1)) and (C1(2) = C2(2));
	end Equals;

	procedure ExplorePaths is
		File : File_Type;
		FirstPath : array (1..FirstWirePathLength) of WireDirection;
		SecondPath : array (1..SecondWirePathLength) of WireDirection;
		c : Character;
		i : Integer;
		CurrentIndex, FirstIndex, SecondIndex : Integer;
		FirstPathCurrentProgress, SecondPathCurrentProgress : Integer;
		FirstCoordinate, SecondCoordinate : Coordinates;
		ClosestIntersection : Intersection;
		AllIntersections : array (1..100) of Intersection;
		AllIntersectionsLength : Integer := 0;
		FirstTotalSteps, SecondTotalSteps, BestCombinedSteps : Integer := 0;
	begin
		-- Get Paths
		Open(File, IN_FILE, "input");
		CurrentIndex := 1;
		FIRST_PATH:
		loop
			Get(File, c);
			case c is
				when 'U' => FirstPath(CurrentIndex).dir := 'U';
				when 'D' => FirstPath(CurrentIndex).dir := 'D';
				when 'L' => FirstPath(CurrentIndex).dir := 'L';
				when 'R' => FirstPath(CurrentIndex).dir := 'R';
				when others => null;
			end case;
			Get(File, i);
			FirstPath(CurrentIndex).len := i;
			exit when End_Of_Line(File);
			Get(File, c);
			CurrentIndex := CurrentIndex+1;
		end loop FIRST_PATH;
		CurrentIndex := 1;
		SECOND_PATH:
		loop
			Get(File, c);
			case c is
				when 'U' => SecondPath(CurrentIndex).dir := 'U';
				when 'D' => SecondPath(CurrentIndex).dir := 'D';
				when 'L' => SecondPath(CurrentIndex).dir := 'L';
				when 'R' => SecondPath(CurrentIndex).dir := 'R';
				when others => null;
			end case;
			Get(File, i);
			SecondPath(CurrentIndex).len := i;
			exit when End_Of_File(File);
			Get(File, c);
			CurrentIndex := CurrentIndex+1;
		end loop SECOND_PATH;
		Close(File);
		-- Explore paths
		FirstCoordinate := (0,0);
		FirstIndex := 1;
		while FirstIndex <= FirstWirePathLength loop
			FirstPathCurrentProgress := 0;
			while FirstPathCurrentProgress < FirstPath(FirstIndex).len loop
				ProgressThrough(FirstPath(FirstIndex).dir, FirstCoordinate);
				FirstTotalSteps := FirstTotalSteps+1;
				SecondCoordinate := (0,0);
				SecondIndex := 1;
				SecondTotalSteps := 0;
				while SecondIndex <= SecondWirePathLength loop
					SecondPathCurrentProgress := 0;
					while SecondPathCurrentProgress < SecondPath(SecondIndex).len loop
						ProgressThrough(SecondPath(SecondIndex).dir, SecondCoordinate);
						SecondTotalSteps := SecondTotalSteps+1;
						if Equals(FirstCoordinate, SecondCoordinate) then
							i := 1;
							while i <= AllIntersectionsLength loop
								exit when Equals(FirstCoordinate, AllIntersections(i).crd);
								i := i+1;
							end loop;
							-- if intersection found for the first time
							if i = AllIntersectionsLength+1 then
								AllIntersections(i).crd := FirstCoordinate;
								-- must be the shortest way to arrive there for both
								AllIntersections(i).firstWireSteps := FirstTotalSteps;
								AllIntersections(i).secondWireSteps := SecondTotalSteps;
								AllIntersectionsLength := i;
							end if;
							if Equals(ClosestIntersection.crd, (0,0)) or (ClosestIntersection.firstWireSteps + ClosestIntersection.secondWireSteps > AllIntersections(i).firstWireSteps + AllIntersections(i).secondWireSteps) then
								ClosestIntersection := AllIntersections(i);
							end if;
							BestCombinedSteps := ClosestIntersection.firstWireSteps + ClosestIntersection.secondWireSteps;
							Put_Line(BestCombinedSteps'Image);
						end if;
--						if Equals(FirstCoordinate, SecondCoordinate) and (Equals(ClosestIntersection, (0,0)) or GetManathanDistance(FirstCoordinate) < GetManathanDistance(ClosestIntersection)) then
--							ClosestIntersection := FirstCoordinate;
--							Put_Line(GetManathanDistance(ClosestIntersection)'Image);
--						end if;
						SecondPathCurrentProgress := SecondPathCurrentProgress+1;
					end loop;
					SecondIndex := SecondIndex+1;
				end loop;
				FirstPathCurrentProgress := FirstPathCurrentProgress+1;
			end loop;
			FirstIndex := FirstIndex+1;
		end loop;
	end ExplorePaths;
end CrossWire;
