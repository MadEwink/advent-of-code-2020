with Ada.Text_IO; use Ada.Text_IO;
with Ada.Command_Line;
with MonitoringStation;

procedure Main is
	Size : Integer := Integer'Value(Ada.Command_Line.Argument(2));
	package CurrSta is new MonitoringStation(Size,Size);
	use CurrSta;
	Map : AsteroidMap := ReadFromFile(Ada.Command_Line.Argument(1));
	X,Y : Natural;
	Xt, Yt : Natural;
begin
	--PrintMap(Map);
	CalculateSights(Map);
	FindBestSight(Map, X,Y);
	Put_Line(X'Image & Y'Image & Map(X,Y)'Image);
	FindDestroyedAsteroid(Map, X,Y, 200, Xt,Yt);
	Put_Line(Xt'Image & Yt'Image);
end Main;
