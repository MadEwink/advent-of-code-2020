with Ada.Containers.Vectors;

package StarsAlign is
	type Coordinates_Type is
		record
			X : Integer;
			Y : Integer;
		end record;
	subtype StarPosition_Type is Coordinates_Type;
	subtype StarVelocity_Type is Coordinates_Type;
	function "+"(Left : IN StarPosition_Type ; Right : IN StarVelocity_Type) return StarPosition_Type;
	procedure PrintCoordinate(C : IN Coordinates_Type);
	type Star_Type is
		record
			Position : StarPosition_Type;
			Velocity : StarVelocity_Type;
		end record;
	package StarVectors is new Ada.Containers.Vectors(Natural, Star_Type);
	procedure ProcessMessageInTheSky(FileName : IN String);
end StarsAlign;
