with Ada.Text_IO; use Ada.Text_IO;

package body InventoryScan is
	procedure AnalyzeWord(Word : IN String ; ContainsTwo, ContainsThree : OUT Boolean) is
		CharacterCounter : CharCount := (others => 0);
	begin
		ContainsTwo := False;
		ContainsThree := False;
		-- Compute each character occurencies
		for c of Word loop
			CharacterCounter(c) := CharacterCounter(c) + 1;
		end loop;
		-- Check if one occurs exactly two or three times
		for c of Word loop
			if (CharacterCounter(c) = 2) then
				ContainsTwo := True;
			elsif (CharacterCounter(c) = 3) then
				ContainsThree := True;
			end if;
			-- no need to look further if both are already found
			exit when ContainsTwo and ContainsThree;
		end loop;
	end;
	function DoScan return Natural is
		Input_File : File_Type;
		Word : Word_Type;
		Last : Natural;
		ContainsTwo, ContainsThree : Boolean;
		NumberOfTwo, NumberOfThree : Natural := 0;
	begin
		Open(Input_File, IN_FILE, "input");
		while not End_Of_File(Input_File) loop
			Get_Line(Input_File, Word, Last);
			AnalyzeWord(Word(Word'First..Last), ContainsTwo, ContainsThree);
			if ContainsTwo then
				NumberOfTwo := NumberOfTwo + 1;
			end if;
			if ContainsThree then
				NumberOfThree := NumberOfThree + 1;
			end if;
		end loop;
		Close(Input_File);
		return NumberOfTwo*NumberOfThree;
	end DoScan;
	procedure ReadWords(Names : IN OUT BoxNames.Vector) is
		Input_File : File_Type;
		Name : BoxName_Type;
	begin
		Open(Input_File, IN_FILE, "input");
		while not End_Of_File(Input_File) loop
			Get_Line(Input_File, Name.Word, Name.Last);
			BoxNames.Append(Names, Name);
		end loop;
		Close(Input_File);
	end ReadWords;
	procedure FindBoxes is
		Names : BoxNames.Vector;
		i,j : Natural;
		Differences : Natural;
	begin
		ReadWords(Names);
		i := BoxNames.First_Index(Names);
		while i /= BoxNames.Last_Index(Names)-1 loop
			j := i+1;
			while j /= BoxNames.Last_Index(Names) loop
				if Names(i).Last = Names(j).Last then
					Differences := 0;
					for k in Names(i).Word'First..Names(i).Last loop
						if Names(i).Word(k) /=Names(j).Word(k) then
							Differences := Differences + 1;
						end if;
						exit when Differences > 1;
					end loop;
					if Differences = 1 then
						Put_Line("Names " & Names(i).Word(Names(i).Word'First..Names(i).Last) & " and " & Names(j).Word(Names(j).Word'First..Names(j).Last));
						Put("Result in : ");
						for k in Names(i).Word'First..Names(i).Last loop
							if Names(i).Word(k) = Names(j).Word(k) then
								Put(Names(i).Word(k));
							end if;
						end loop;
						New_Line;
					end if;
				end if;
				j := j+1;
			end loop;
			i := i+1;
		end loop;
	end FindBoxes;
end InventoryScan;
