with Ada.Text_IO; use Ada.Text_IO;
with Ada.Containers.Vectors;

package body Slicer is
	procedure ParseClaimString(ClaimString : IN String ; Last : IN Natural ; Claim : OUT Claim_Type) is
		procedure ReadNumber(Line : IN String ; Last : IN Natural ; Number : OUT Natural ; IndexInc : OUT Natural) is
		begin
			Number := 0;
			IndexInc := 0;
			for c of Line(Line'First..Last) loop
				Number := Number * 10;
				IndexInc := IndexInc + 1;
				case c is
					when '1' => Number := Number +1;
					when '2' => Number := Number +2;
					when '3' => Number := Number +3;
					when '4' => Number := Number +4;
					when '5' => Number := Number +5;
					when '6' => Number := Number +6;
					when '7' => Number := Number +7;
					when '8' => Number := Number +8;
					when '9' => Number := Number +9;
					when '0' => Number := Number +0;
					when others =>
						Number := Number / 10;
						exit;
				end case;
			end loop;
		end ReadNumber;
		Index : Natural := ClaimString'First+1;
		IndexInc : Natural;
	begin
		ReadNumber(ClaimString(Index..Last), Last, Claim.Id, IndexInc);
		Index := Index + IndexInc + 2;
		ReadNumber(ClaimString(Index..Last), Last, Claim.Offset.X, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(ClaimString(Index..Last), Last, Claim.Offset.Y, IndexInc);
		Index := Index + IndexInc + 1;
		ReadNumber(ClaimString(Index..Last), Last, Claim.Size.X, IndexInc);
		Index := Index + IndexInc;
		ReadNumber(ClaimString(Index..Last), Last, Claim.Size.Y, IndexInc);
	end ParseClaimString;
	procedure ReadAllClaims(Claims : IN OUT ClaimVectors.Vector) is
		Input_File : File_Type;
		Line : String(1..30) := (others => ' ');
		Last : Natural;
		Claim : Claim_Type;
	begin
		Open(Input_File, IN_FILE, "input");
		while not End_Of_File(Input_File) loop
			Get_Line(Input_File, Line, Last);
			ParseClaimString(Line, Last, Claim);
			ClaimVectors.Append(Claims, Claim);
		end loop;
		Close(Input_File);
	end ReadAllClaims;
	procedure PrintClaim(Claim : IN Claim_Type) is
	begin
		Put_Line("# " & Claim.Id'Image & " @ " & Claim.Offset.X'Image & "," & Claim.Offset.Y'Image & ": " & Claim.Size.X'Image & "x" & CLaim.Size.Y'Image);
	end PrintClaim;
	procedure PrintAllClaims(Claims : IN ClaimVectors.Vector) is
	begin
		for Index in ClaimVectors.First_Index(Claims)..ClaimVectors.Last_Index(Claims) loop
			PrintClaim(Claims(Index));
		end loop;
	end PrintAllClaims;
	procedure FindMaxXY(Claims : IN ClaimVectors.Vector; MaxX, MaxY : OUT Natural) is
	begin
		MaxX := 0;
		MaxY := 0;
		for Index in ClaimVectors.First_Index(Claims)..ClaimVectors.Last_Index(Claims) loop
			if Claims(Index).Offset.X + Claims(Index).Size.X > MaxX then
				MaxX := Claims(Index).Offset.X + Claims(Index).Size.X;
			end if;
			if Claims(Index).Offset.Y + Claims(Index).Size.Y > MaxY then
				MaxY := Claims(Index).Offset.Y + Claims(Index).Size.Y;
			end if;
		end loop;
	end FindMaxXY;
	procedure CountOverlap(Claims : IN ClaimVectors.Vector; MaxX, MaxY : IN Natural; OverlapingCount : OUT Natural) is
		Count : array (1..MaxX, 1..MaxY) of Natural := (others => (others=> 0) );
		procedure Draw is
		begin
			for Y in 1..MaxY loop
				for X in 1..MaxX loop
					Put(Count(X,Y)'Image);
				end loop;
				New_Line;
			end loop;
		end Draw;
	begin
		OverlapingCount := 0;
		for Index in ClaimVectors.First_Index(Claims)..ClaimVectors.Last_Index(Claims) loop
			for Y in 1..Claims(Index).Size.Y loop
				for X in 1..Claims(Index).Size.X loop
					Count(X+Claims(Index).Offset.X, Y+Claims(Index).Offset.Y) := Count(X+Claims(Index).Offset.X, Y+Claims(Index).Offset.Y) + 1;
				end loop;
			end loop;
		end loop;
		for I of Count loop
			if I > 1 then
				OverlapingCount := OverlapingCount + 1;
			end if;
		end loop;
		-- Find Claim that does not overlap
		declare
			HasOverlap : Boolean;
		begin
			for Index in ClaimVectors.First_Index(Claims)..ClaimVectors.Last_Index(Claims) loop
				HasOverlap := False;
				Explore_Claim:
				for Y in 1..Claims(Index).Size.Y loop
					for X in 1..Claims(Index).Size.X loop
						-- if there is an overlap on this claim
						if Count(X+Claims(Index).Offset.X, Y+Claims(Index).Offset.Y) > 1 then
							HasOverlap := True;
							exit Explore_Claim;
						end if;
					end loop;
				end loop Explore_Claim;
				if not HasOverlap then
					Put_Line(Claims(Index).Id'Image);
					return;
				end if;
			end loop;
		end;
	end CountOverlap;
end Slicer;
