-----------------------------
-- main.adb
-- Contains everything used 
-- for the first day of 
-- Advent Of Code 2018
-----------------------------

with CHRONAL_CALIBRATION;
with ADA.TEXT_IO;
use ADA.TEXT_IO;

procedure MAIN is
	FILE_INPUT : FILE_TYPE;
	FILE_OUTPUT : FILE_TYPE;
	VALUE : INTEGER;
	FREQUENCY : INTEGER;
	FOUND : BOOLEAN;
begin
	FREQUENCY := 0;
	OPEN(FILE_INPUT,  IN_FILE, "input");
	SET_OUTPUT(STANDARD_OUTPUT);
	if not(CHRONAL_CALIBRATION.ADD_FREQUENCY(0)) then
		raise CHRONAL_CALIBRATION.EXISTING_FILE;
	end if;
	FIND :
	loop
		while not(END_OF_FILE(FILE_INPUT)) loop
			CHRONAL_CALIBRATION.READ_INPUT(FILE_INPUT, VALUE);
			FREQUENCY := FREQUENCY + VALUE;
			FOUND := not(CHRONAL_CALIBRATION.ADD_FREQUENCY(FREQUENCY));
			exit FIND when FOUND;
		end loop;
		RESET(FILE_INPUT, IN_FILE);
	end loop FIND;
	CLOSE(FILE_INPUT);
	PUT_LINE("First reached twice :" & INTEGER'IMAGE(FREQUENCY));
end;
