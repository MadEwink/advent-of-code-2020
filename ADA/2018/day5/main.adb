with Ada.Text_IO; use Ada.Text_IO;
with AlchemicalReduction; use AlchemicalReduction;
with Ada.Containers; use Ada.Containers;

procedure Main is
	Units : UnitVectors.Vector;
	Result : Count_Type;
begin
	ReadUnits("input", Units);
	SearchProblem(Units, Result);
	Put_Line(Result'Image);
end Main;
