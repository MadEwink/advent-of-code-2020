with Ada.Containers.Vectors;
with Ada.Containers; use Ada.Containers;

package AlchemicalReduction is
	subtype Units_Type is Character range 'A'..'z';
	package UnitVectors is new Ada.Containers.Vectors(Natural, Units_Type);
	procedure ReadUnits(FileName : IN String ; Units : OUT UnitVectors.Vector);
	procedure PrintUnits(Units : IN UnitVectors.Vector);
	procedure SimplifyPolymer(Units : IN OUT UnitVectors.Vector ; HasChanged : OUT Boolean);
	procedure SearchProblem(Units : IN UnitVectors.Vector ; BestLength : OUT Count_Type);
end AlchemicalReduction;
