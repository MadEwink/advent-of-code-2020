with Ada.Containers.Vectors;
with Ada.Containers.Multiway_Trees;

package MemoryManeuver is
	package MetadataVectors is new Ada.Containers.Vectors(Natural, Natural);
	type Node_Type is
		record
			ChildNumber : Natural := 0;
			MetadataNumber : Positive := 1;
			Value : Natural := 0;
			Metadatas : MetadataVectors.Vector;
		end record;
	package Node_Trees is new Ada.Containers.Multiway_Trees(Node_Type);
	procedure ReadNodes(FileName : IN String ; Nodes : OUT Node_Trees.Tree);
	procedure PrintNodes(Nodes : IN Node_Trees.Tree);
	procedure ComputeMetadataSum(FileName : IN String);
	procedure ComputeRootValue(FileName : IN String);
end MemoryManeuver;
