with Ada.Text_IO; use Ada.Text_IO;

package body ChronalCoordinates is
	use CoordinatesVectors;
	procedure ReadCoordinates(FileName : IN String ; Coordinates : OUT Vector) is
		Input_File : File_Type;
		Coordinate : Coordinates_Type;
		Char : Character;
		package Int_IO is new Integer_IO(Natural);
	begin
		Open(Input_File, IN_FILE, FileName);
		while not End_Of_File(Input_File) loop
			Int_IO.Get(Input_File, Coordinate.X);
			Get(Input_File, Char); -- ','
			Get(Input_File, Char); -- ' '
			Int_IO.Get(Input_File, Coordinate.Y);
			Append(Coordinates, Coordinate);
		end loop;
		Close(Input_File);
	end ReadCoordinates;
	procedure PrintCoordinate(Coordinate : IN Coordinates_Type) is
	begin
		Put_Line(Coordinate.X'Image & "," & Coordinate.Y'Image);
	end PrintCoordinate;
	procedure PrintCoordinates(Coordinates : IN Vector) is
	begin
		for Coordinate of Coordinates loop
			PrintCoordinate(Coordinate);
		end loop;
	end PrintCoordinates;
	function GetManhattanDistance(a,b : Coordinates_Type) return Natural is (abs (a.X - b.X) + abs (a.Y-b.Y));
	procedure GetClosestCoordinateIndex(Coordinates : IN Vector ; Coordinate : IN Coordinates_Type ; MinIndex : OUT Natural) is
		Distance : Natural;
		MinDistance : Natural;
		IsTied : Boolean := False;
	begin
		MinIndex := First_Index(Coordinates);
		MinDistance := GetManhattanDistance(Coordinates(MinIndex), Coordinate);
		for Index in First_Index(Coordinates)..Last_Index(Coordinates) loop
			Distance := GetManhattanDistance(Coordinates(Index), Coordinate);
			if Distance < MinDistance then
				MinIndex := Index;
				MinDistance := Distance;
				IsTied := False;
			elsif Distance = MinDistance then
				IsTied := True;
			end if;
		end loop;
		if IsTied then
			MinIndex := Last_Index(Coordinates)+1;
		end if;
	end GetClosestCoordinateIndex;
	procedure ExploreRing(Coordinates : IN Vector ; CurrentObjectIndex : IN Natural ; RingRadius : IN Natural ; MinCoord, MaxCoord : IN Coordinates_Type ; OwnArea : IN OUT Natural ; NewArea : OUT Boolean) is
		CurrentCoordinate : Coordinates_Type;
		ClosestCoordinateIndex : Natural;
		function DoExplore return Boolean is
		begin
			GetClosestCoordinateIndex(Coordinates, CurrentCoordinate, ClosestCoordinateIndex);
			if (ClosestCoordinateIndex = CurrentObjectIndex) then
				OwnArea := OwnArea + 1;
				NewArea := True;
				if CurrentCoordinate.X <= MinCoord.X or CurrentCoordinate.Y <= MinCoord.Y or CurrentCoordinate.X >= MaxCoord.X or CurrentCoordinate.Y >= MaxCoord.Y then
					-- we reach a border, the area is infinite
					OwnArea := 0;
					NewArea := False;
				end if;
			end if;
			return True;
		end DoExplore;
	begin
		CurrentCoordinate.X := Coordinates(CurrentObjectIndex).X + RingRadius;
		CurrentCoordinate.Y := Coordinates(CurrentObjectIndex).Y + RingRadius;
		NewArea := False;
		-- go minus X
		for X in 1..2*RingRadius loop
			CurrentCoordinate.X := CurrentCoordinate.X - 1;
			exit when (not DoExplore) or CurrentCoordinate.X <= MinCoord.X;
		end loop;
		-- go minus Y
		for Y in 1..2*RingRadius loop
			CurrentCoordinate.Y := CurrentCoordinate.Y - 1;
			exit when (not DoExplore) or CurrentCoordinate.Y <= MinCoord.Y;
		end loop;
		-- go plus X
		for X in 1..2*RingRadius loop
			CurrentCoordinate.X := CurrentCoordinate.X + 1;
			exit when (not DoExplore) or CurrentCoordinate.X >= MaxCoord.X;
		end loop;
		-- go plus Y
		for Y in 1..2*RingRadius loop
			CurrentCoordinate.Y := CurrentCoordinate.Y + 1;
			exit when (not DoExplore) or CurrentCoordinate.Y >= MaxCoord.Y;
		end loop;
	end ExploreRing;
	procedure FindMinMaxCoordinates(Coordinates : IN Vector ; Min,Max : OUT Coordinates_Type) is
	begin
		Min := Coordinates(First_Index(Coordinates));
		Max := Min;
		for Coordinate of Coordinates loop
			if Coordinate.X < Min.X then
				Min.X := Coordinate.X;
			end if;
			if Coordinate.Y < Min.Y then
				Min.Y := Coordinate.Y;
			end if;
			if Coordinate.X > Max.X then
				Max.X := Coordinate.X;
			end if;
			if Coordinate.Y > Max.Y then
				Max.Y := Coordinate.Y;
			end if;
		end loop;
		-- take some margins :
		Min.X := Min.X - 1;
		Min.Y := Min.Y - 1;
		Max.X := Max.X + 1;
		Max.Y := Max.Y + 1;
	end FindMinMaxCoordinates;
	procedure FindLargestArea(FileName : IN String) is
		Coordinates : Vector;
		CurrentArea, LargestArea : Natural := 0;
		CurrentRadius : Natural;
		HasNewArea : Boolean;
		MinCoord, MaxCoord : Coordinates_Type;
	begin
		ReadCoordinates(FileName, Coordinates);
		FindMinMaxCoordinates(Coordinates, MinCoord, MaxCoord);
		for Index in First_Index(Coordinates)..Last_Index(Coordinates) loop
			CurrentRadius := 1;
			CurrentArea := 1; -- include self area
			loop
				ExploreRing(Coordinates, Index, CurrentRadius, MinCoord, MaxCoord, CurrentArea, HasNewArea);
				exit when not HasNewArea;
				CurrentRadius := CurrentRadius + 1;
			end loop;
			if (CurrentArea > LargestArea) then
				LargestArea := CurrentArea;
			end if;
		end loop;
		Put_Line(LargestArea'Image);
	end FindLargestArea;
	procedure ComputeTotalDistanceToAll(Coordinates : IN Vector ; Coordinate : IN Coordinates_Type ; TotalDistance : OUT Natural) is
	begin
		TotalDistance := 0;
		for CurrentCoordinate of Coordinates loop
			TotalDistance := TotalDistance + GetManhattanDistance(Coordinate, CurrentCoordinate);
		end loop;
	end ComputeTotalDistanceToAll;
	procedure FindAreaLessTenThousand(FileName : IN String) is
		Coordinates : Vector;
		MinCoord, MaxCoord : Coordinates_Type;
		CurrentCoordinate : Coordinates_Type;
		CurrentDistance : Natural;
		Area : Natural := 0;
	begin
		ReadCoordinates(FileName, Coordinates);
		FindMinMaxCoordinates(Coordinates, MinCoord, MaxCoord);
		for Y in MinCoord.Y..MaxCoord.Y loop
			CurrentCoordinate.Y := Y;
			for X in MinCoord.X..MaxCoord.X loop
				CurrentCoordinate.X := X;
				ComputeTotalDistanceToAll(Coordinates, CurrentCoordinate, CurrentDistance);
				if CurrentDistance < 10000 then
					Area := Area + 1;
				end if;
			end loop;
		end loop;
		Put_Line(Area'Image);
	end FindAreaLessTenThousand;
end ChronalCoordinates;
