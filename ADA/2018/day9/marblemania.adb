with Ada.Text_IO; use Ada.Text_IO;

package body MarbleMania is
	use NaturalVectors;
	procedure InsertMarble(Circle : IN OUT Vector ; CurrentMarblePosition : IN OUT Natural ; NewMarble : IN Natural) is
	begin
		-- make sure we don't insert a multiple of 23
		if (NewMarble / 23) * 23 = NewMarble then
			return;
		end if;
		CurrentMarblePosition := CurrentMarblePosition+2;
		while CurrentMarblePosition > Last_Index(Circle)+1 loop
			CurrentMarblePosition := CurrentMarblePosition - (Last_Index(Circle)+1);
		end loop;
		Insert(Container =>Circle,
			Before => CurrentMarblePosition,
			New_Item => NewMarble);
	end InsertMarble;
	procedure UpdateScore(Circle : IN OUT Vector ; CurrentMarblePosition : IN OUT Natural ; NewMarble,CurrentPlayer : IN Natural ; Score : IN OUT Score_Type) is
	begin
		--make sure we do have a multiple of 23
		if (NewMarble / 23) * 23 /= NewMarble then
			return;
		end if;
		Score(CurrentPlayer) := Score(CurrentPlayer)+NewMarble;
		while CurrentMarblePosition < 7 loop
			CurrentMarblePosition := CurrentMarblePosition + (Last_Index(Circle)+1);
		end loop;
		CurrentMarblePosition := CurrentMarblePosition - 7;
		Score(CurrentPlayer) := Score(CurrentPlayer)+Circle(CurrentMarblePosition);
		Delete(Container => Circle,
			Index => CurrentMarblePosition);
	end UpdateScore;
	procedure PrintBestScore(Scores : IN Score_Type) is
		MaxScore : Natural := 0;
	begin
		for Score of Scores loop
			if MaxScore < Score then
				MaxScore := Score;
			end if;
		end loop;
		Put_Line(MaxScore'Image);
	end PrintBestScore;
	procedure PlayGame(PlayerNumber, LastMarbleNumber : IN Natural) is
		Circle : Vector;
		Score : Score_Type(1..PlayerNumber) := (others => 0);
		CurrentMarblePosition : Natural := 0;
		NewMarble : Natural := 0;
		CurrentPlayer : Natural := 1;
	begin
		Circle.Append(NewMarble);
		while NewMarble < LastMarbleNumber loop
			NewMarble := NewMarble+1;
			InsertMarble(Circle, CurrentMarblePosition, NewMarble);
			UpdateScore(Circle, CurrentMarblePosition, NewMarble, CurrentPlayer, Score);
			CurrentPlayer := CurrentPlayer + 1;
			if CurrentPlayer > PlayerNumber then
				CurrentPlayer := CurrentPlayer - PlayerNumber;
			end if;
		end loop;
		PrintBestScore(Score);
	end PlayGame;
end MarbleMania;
