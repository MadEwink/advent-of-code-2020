def insert_marble(circle, current_position, new_marble):
    if (new_marble % 23 == 0):
        return current_position
    current_position += 2 
    while (current_position > len(circle)):
        current_position -= len(circle)
    if (current_position == len(circle)):
        circle.append(new_marble)
    else:
        circle.append(circle[-1])
        for i in range(len(circle)-2, current_position-1, -1):
            circle[i+1] = circle[i]
        circle[current_position] = new_marble
    return current_position

def update_score(circle, score, current_position, new_marble, current_player):
    if (new_marble % 23 != 0):
        return current_position
    current_position -= 7
    while current_position < 0:
        current_position += len(circle)
    score[current_player] += new_marble + circle[current_position]
    circle.remove(circle[current_position])
    return current_position

def playGame(nb_players, last_marble):
    circle = [0]
    score = [0 for i in range(nb_players)]
    current_player = 1
    current_position = 0
    new_marble = 0
    while new_marble < last_marble:
        new_marble += 1
        current_position = insert_marble(circle, current_position, new_marble)
        current_position = update_score(circle, score, current_position, new_marble, current_player)
        current_player += 1
        current_player %= nb_players
    print(max(score))

if (__name__=="__main__"):
    playGame(9,25)
    playGame(10, 1618)
    playGame(13, 7999)
    playGame(17, 1104)
    playGame(21, 6111)
    playGame(30, 5807)
    playGame(470, 72170)
    playGame(470, 72170*100)
