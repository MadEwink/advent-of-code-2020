with ADA.TEXT_IO; use ADA.TEXT_IO;

package body Submarine is
	procedure ReadCommand(File : IN File_Type; Command : OUT CommandType) is
		Line : String(1..16);
		LineLength : Integer;
		InstructionEndPos : Integer;
	begin
		GET_LINE(File, Line, LineLength);
		InstructionEndPos := 0;
		for Index in 1..LineLength loop
			if Line(Index) = ' ' then
				InstructionEndPos := Index-1;
				exit;
			end if;
		end loop;
		if InstructionEndPos = 0 then
			raise InputError;
		end if;

		Command.Value := Integer'Value(Line(InstructionEndPos+1..LineLength));

		if Line(1..InstructionEndPos) = "forward" then
			Command.Direction := Forward;
			return;
		elsif Line(1..InstructionEndPos) = "down" then
			Command.Direction := Down;
			return;
		elsif Line(1..InstructionEndPos) = "up" then
			Command.Direction := Up;
			return;
		end if;

		raise InputError;
	end ReadCommand;
	
	procedure ProcessCommand(Submarine : IN OUT SubmarineType; Command : IN CommandType) is
	begin
		if Command.Direction = Forward then
			Submarine.Coordinates.Horizontal := Submarine.Coordinates.Horizontal + Command.Value;
			Submarine.Coordinates.Depth := Submarine.Coordinates.Depth + (Command.Value * Submarine.Aim);
		elsif Command.Direction = Down then
			Submarine.Aim := Submarine.Aim + Command.Value;
		else
			Submarine.Aim := Submarine.Aim - Command.Value;
		end if;
	end ProcessCommand;
end Submarine;
