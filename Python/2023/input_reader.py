class InputReader:
    def __init__(self, file_name : str) -> None:
        self.file_name = file_name
        self.read()

    def read(self) -> None:
        with open(self.file_name) as file:
            self.lines = file.readlines()
        for i in range(len(self.lines)):
            self.lines[i] = self.lines[i].strip()