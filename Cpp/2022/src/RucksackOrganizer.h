#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "Rucksack.h"

class RucksackOrganizer {
    public:
    RucksackOrganizer(string fileName);
    int getCommonPrioritySum();
    int getBadgePrioritySum();
    private:
    vector<Rucksack> rucksacks;
    void readFromFile(string fileName);
    static const int GROUP_SIZE = 3;
    int getGroupNumber();
    Rucksack getGroupRucksack(int group, int member);
    char findBadgeFromGroup(int group);
    bool recursiveSearchBadge(int group, int member, char badge);
};
