#include <iostream>
#include <string>
#include "Crane.h"

int main(int argc, char** argv) {
    string fileName("input/input-5");
    if (argc > 1) {
        fileName = argv[1];
    }

    StorageSite site(fileName);
    Crane crane(fileName);

    cout << "Init top crates marks : " << site.getTopMarks() << endl;

    crane.operate(site);

    cout << "Top crates marks : " << site.getTopMarks() << endl;

    return 0;
}
