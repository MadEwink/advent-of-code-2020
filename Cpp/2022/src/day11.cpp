#include <iostream>
#include <string>
#include "MonkeyManager.h"

int main(int argc, char** argv) {
    string fileName("input/input-11");
    if (argc > 1) {
        fileName = argv[1];
    }

    int nbTurns = 20;
    if (argc > 2) {
        nbTurns = stoi(argv[2]);
    }

    int relievedValue = 3;
    if (argc > 3) {
        relievedValue = stoi(argv[3]);
    }

    MonkeyManager monkeyManager(fileName);

    long businessValue = monkeyManager.getMonkeyBusinessValue(nbTurns, 2, relievedValue);

    cout << "Business value of two most actives : " << businessValue << endl;

    return 0;
}
