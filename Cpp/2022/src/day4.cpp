#include <iostream>
#include <string>
#include "CampOrganizer.h"

int main(int argc, char** argv) {
    string fileName("input/input-4");
    if (argc > 1) {
        fileName = argv[1];
    }

    CampOrganizer campOrganizer(fileName);

    int totalContaining = campOrganizer.countFullyContainingAssignments();

    int totalOverlapping = campOrganizer.countOverlappingAssignments();

    cout << "Total containing pairs : " << totalContaining << endl;

    cout << "Total overlapping pairs : " << totalOverlapping << endl;

    return 0;
}
