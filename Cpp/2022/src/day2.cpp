#include <iostream>
#include <string>
#include "StrategyGuide.h"

int main(int argc, char** argv) {
    string fileName("input/input-2");
    if (argc > 1) {
        fileName = argv[1];
    }

    StrategyGuide stratGuide(fileName);

    int score = stratGuide.computeScore();

    cout << "Score : " << score << endl;

    return 0;
}
