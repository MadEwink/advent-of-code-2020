class Operation {
    public:
    Operation(int nbMoves, int startStackId, int endStackId);
    int getNbMoves();
    int getStartStackId();
    int getEndStackId();
    private:
    int nbMoves;
    int startStackId;
    int endStackId;
};
