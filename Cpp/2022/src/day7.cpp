#include <iostream>
#include <string>
#include "FileSystemParser.h"

int main(int argc, char** argv) {
    string fileName("input/input-7");
    if (argc > 1) {
        fileName = argv[1];
    }

    FileSystemParser fileSystemParser;
    fileSystemParser.parseFileSystem(fileName);
    int totalSize = fileSystemParser.getTotalSize();

    cout << "Total size : " << totalSize << endl;

    int lessThanHundredThousand = fileSystemParser.getSumOfSizeLessThan(100000);

    cout << "Sum of sizes less than 100000 : " << lessThanHundredThousand << endl;

    int minSizeToFree = fileSystemParser.getSmallestDirectoryForSpace(70000000, 30000000);

    cout << "Min size to delete to have 30000000/70000000 : " << minSizeToFree << endl;

    return 0;
}
