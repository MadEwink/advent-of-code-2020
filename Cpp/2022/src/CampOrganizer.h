#include <string>
#include <vector>

#include "SectionAssignment.h"

using namespace std;

class CampOrganizer{
    public:
    CampOrganizer(string fileName);
    int countFullyContainingAssignments();
    int countOverlappingAssignments();
    private:
    vector<SectionAssignment> assignments;
    void readFromFile(string inputFile);
};

