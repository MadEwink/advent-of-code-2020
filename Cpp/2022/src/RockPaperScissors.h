enum RockPaperScissors {
    Rock,
    Paper,
    Scissors
};

class RPSGame {
    public:
    // returns -1 if a wins, 1 if b wins, and 0 on draw
    static int resolve(RockPaperScissors a, RockPaperScissors b);
};
