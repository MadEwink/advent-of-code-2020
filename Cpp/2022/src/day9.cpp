#include <iostream>
#include <string>
#include "Rope.h"

int main(int argc, char** argv) {
    string fileName("input/input-9");
    if (argc > 1) {
        fileName = argv[1];
    }

    Rope rope(fileName);

    int nbTailVisited = rope.countVisitedTailPositions();

    cout << "Number of positions visited by tail : " << nbTailVisited << endl;

    nbTailVisited = rope.countVisitedTailPositions(10);

    cout << "Same for length 10 : " << nbTailVisited << endl;

    return 0;
}
