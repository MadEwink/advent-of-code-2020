#include <vector>
#include "Snack.h"

using namespace std;

class Elf {
    public:
    Elf();
    void addSnack(Snack snack);
    int getTotalCalories();
    private:
    vector<Snack> snacks;
};
