#include <iostream>
#include <string>
#include "CathodeRay.h"

int main(int argc, char** argv) {
    string fileName("input/input-10");
    if (argc > 1) {
        fileName = argv[1];
    }

    CathodeRay cathodeRay;

    int signalSum = cathodeRay.getSignalStrengthSum(fileName, 20, 40);

    cout << "Signal strength sum : " << signalSum << endl;

    return 0;
}
