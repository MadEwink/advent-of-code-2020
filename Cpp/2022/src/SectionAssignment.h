class SectionAssignment {
    public:
    SectionAssignment(int minId, int maxId);
    bool contains(SectionAssignment other);
    bool overlaps(SectionAssignment other);
    private:
    int minSectionId;
    int maxSectionId;
};
