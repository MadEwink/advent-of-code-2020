class Crate {
    public:
    Crate(char mark);
    char getMark();
    private:
    char mark;
};
