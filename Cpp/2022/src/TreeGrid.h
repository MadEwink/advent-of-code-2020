#include <string>
#include <vector>

using namespace std;

class TreeGrid {
    public:
    TreeGrid(string fileName);
    int countVisible();
    int bestScenicView();
    private:
    int gridWidth;
    int gridHeight;
    vector<vector<int>> treeGrid;
    void readFromFile(string fileName);
    bool isVisible(int x, int y);
    bool isVisibleFromLeft(int height, int x, int y);
    bool isVisibleFromRight(int height, int x, int y);
    bool isVisibleFromUp(int height, int x, int y);
    bool isVisibleFromDown(int height, int x, int y);
    int scenicScore(int x, int y);
    int countViewUp(int height, int x, int y);
    int countViewDown(int height, int x, int y);
    int countViewLeft(int height, int x, int y);
    int countViewRight(int height, int x, int y);
};
