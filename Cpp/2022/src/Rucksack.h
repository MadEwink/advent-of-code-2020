#include <string>

using namespace std;

class Rucksack {
    public:
    Rucksack(string items);
    int getItemNumber();
    int getCompartimentSize();
    static const int COMPARTIMENT_NB = 2;
    char getCommonItem();
    char getItem(int i);
    private:
    string items;
    char getCompartmentItem(int compartiment, int i);
    bool recursiveSearchItem(int compartiment, char item);
};
