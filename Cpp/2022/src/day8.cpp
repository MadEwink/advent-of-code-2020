#include <iostream>
#include <string>
#include "TreeGrid.h"

int main(int argc, char** argv) {
    string fileName("input/input-8");
    if (argc > 1) {
        fileName = argv[1];
    }

    TreeGrid treeGrid(fileName);

    int visible = treeGrid.countVisible();

    cout << "Visible trees : " << visible << endl;

    int bestScore = treeGrid.bestScenicView();

    cout << "Best scenic view : " << bestScore << endl;

    return 0;
}
