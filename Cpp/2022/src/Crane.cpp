#include "Crane.h"

#include <iostream>
#include <fstream>

Crane::Crane(string fileName) {
    readInstructions(fileName);
}

void Crane::operate(StorageSite& site) {
    for (int i = 0 ; i < operations.size() ; i++) {
        Operation operation = operations[i];
        CrateStack& from = site.getStack(operation.getStartStackId());
        CrateStack& to = site.getStack(operation.getEndStackId());
        CrateStack tempStack;
        for (int j = 0 ; j < operation.getNbMoves() ; j++) {
            tempStack.addCrate(from.popCrate());
        }
        for (int j = 0 ; j < operation.getNbMoves() ; j++) {
            to.addCrate(tempStack.popCrate());
        }
    }
}

void Crane::readInstructions(string fileName) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw("Could not open input file");
    }

    string line;

    while (getline(inputFile, line)) {
        if (line.find("move") == string::npos) {
            continue;
        }

        int movesFirstIndex = 5;
        int movesLength = line.find("from") - movesFirstIndex - 1;
        int fromFirstIndex = movesFirstIndex + movesLength + 6;
        int fromLength = line.find("to") - fromFirstIndex - 1;
        int toFirstIndex = fromFirstIndex + fromLength + 4;

        int nbMoves = stoi(line.substr(movesFirstIndex, movesLength));
        int from = stoi(line.substr(fromFirstIndex, fromLength));
        int to = stoi(line.substr(toFirstIndex));

        operations.push_back(Operation(nbMoves, from, to));
    }
}
