#include <string>
#include "FolderNode.h"

class FileSystemParser {
    public:
    FileSystemParser();
    void parseFileSystem(string fileName);
    int getTotalSize();
    int getSumOfSizeLessThan(int maxSizeToConsider);
    int getSmallestDirectoryForSpace(int totalSpace, int desiredAvailable);
    private:
    FolderNode root;
    FolderNode* currentFolder;
    void parseCommand(string commandLine);
    void parseFile(string fileLine);
};
