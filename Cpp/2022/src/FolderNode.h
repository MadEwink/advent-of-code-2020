#include "FileSystemNode.h"

#include <vector>

class FolderNode : public FileSystemNode {
    public:
    FolderNode(string name, FolderNode* parent);
    FolderNode(string name);
    ~FolderNode();
    int getSize() override;
    int getSizeIfLessThan(int maxSize);
    int getMinBiggerThan(int min);
    FolderNode* getOrCreateChildFolder(string childName);
    void addChildFolder(string newChildName);
    void addChildFile(string newChildName, int size);
    FolderNode* getParent();
    private:
    vector<FileSystemNode*> children;
    FolderNode* parent;
};
