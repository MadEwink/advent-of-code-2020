#include <vector>
#include <string>
#include "StorageSite.h"
#include "Operation.h"

using namespace std;

class Crane {
    public:
    Crane(string fileName);
    void operate(StorageSite& site);
    private:
    vector<Operation> operations;
    void readInstructions(string fileName);
};
