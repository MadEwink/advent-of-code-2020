#include "CathodeRay.h"

#include <iostream>
#include <fstream>
#include <cstdlib>

int CathodeRay::getSignalStrengthSum(string fileName, int initStep, int stepSize) {
    ifstream inputFile(fileName);

    if (!inputFile.is_open()) {
        throw "Could not open input file";
    }

    int curStep = 0;
    int curValue = 1;
    int curCycle = 0;
    int signalStrengthSum = 0;

    string curLine;
    string curDraw = "";

    while (getline(inputFile, curLine)) {
        if (curLine == "noop") {
            timeBuffer ++;
        } else { 
            nextValueChange = stoi(curLine.substr(curLine.find(' ')+1));
            timeBuffer +=2;
        }
        
        while (timeBuffer > 0) {
            // draw
            int screenPos = curCycle % stepSize;
            if (abs(screenPos - curValue) <= 1) {
                curDraw.push_back('#');
            } else {
                curDraw.push_back('.');
            }
            if (screenPos == stepSize-1) {
                cout << curDraw << endl;
                curDraw.clear();
            }
            // increment cycle
            curCycle ++;
            timeBuffer --;
            // calculate signal strength
            if (curCycle == initStep+stepSize*curStep) {
                signalStrengthSum += (initStep+stepSize*curStep)*curValue;
                curStep ++;
            }
            // apply value change
            if (timeBuffer == 0) {
                curValue += nextValueChange;
                nextValueChange = 0;
            }
        }
    }

    return signalStrengthSum;
}
