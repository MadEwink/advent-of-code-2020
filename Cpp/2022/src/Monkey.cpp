#include "Monkey.h"

Monkey::Monkey() :
    operationType(Addition),
    operationValue(0),
    testValue(0),
    targetTrue(0),
    targetFalse(0),
    totalItemsInspected(0)
{}

void Monkey::grabItem(long item) {
    items.push_front(item);;
}

void Monkey::init(MonkeyOperationType operationType, int operationValue, int testValue, int targetTrue, int targetFalse) {
    this->operationType = operationType;
    this->operationValue = operationValue;
    this->testValue = testValue;
    this->targetTrue = targetTrue;
    this->targetFalse = targetFalse;
}

bool Monkey::hasItemsLeft() {
    return items.size() > 0;
}

void Monkey::inspectItem(int relievedValue, int globalMod) {
    totalItemsInspected ++;
    long& item = items.back();

    long curItem = item;

    // apply 
    switch(operationType) {
        case Addition:
            curItem += operationValue;
            break;
        case Multiplication:
            curItem *= operationValue;
            break;
        case Double:
            curItem += curItem;
            break;
        case Square:
            curItem *= curItem;
            break;
        default:
            throw "Invalid operation";
    }

    if (relievedValue > 0) {
        // apply relieved operation
        curItem /= relievedValue;
    } else {
        curItem = curItem % globalMod;
    }

    item = curItem;
}

bool Monkey::testItem() {
    return (items.back() % testValue) == 0;
}

long Monkey::popItem() {
    long item = items.back();
    items.pop_back();
    return item;
}

int Monkey::getTotalItemsInspected() {
    return totalItemsInspected;
}

int Monkey::getTargetTrue() {
    return targetTrue;
}

int Monkey::getTargetFalse() {
    return targetFalse;
}

