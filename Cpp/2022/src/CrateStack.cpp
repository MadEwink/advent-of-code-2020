#include "CrateStack.h"

CrateStack::CrateStack() {}

void CrateStack::initAddCrate(char mark) {
    crates.emplace_back(mark);
}

Crate CrateStack::popCrate() {
    Crate c = crates.front();
    crates.pop_front();
    return c;
}

void CrateStack::addCrate(Crate crate) {
    crates.push_front(crate);
}

char CrateStack::getTopMark() {
    return crates.front().getMark();
}
