#include "FileSystemNode.h"

class FileNode : public FileSystemNode {
    public:
    FileNode(string name, int size);
    int getSize() override;
    private:
    int size;
};
