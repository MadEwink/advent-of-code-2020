using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

class Day15 : Solver {
    enum Direction {
        Up,
        Down,
        Left,
        Right,
    }

    private static char WALL_SYMBOL = '#';
    private static char BOX_SYMBOL = 'O';
    private static char ROBOT_SYMBOL = '@';
    private static char EMPTY_SYMBOL = '.';
    private static char WBL_SYMBOL = '[';
    private static char WBR_SYMBOL = ']';

    class Position {
        public int X;
        public int Y;

        public Position(int x, int y) {
            X = x;
            Y = y;
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || obj.GetType() != GetType()) {
                return false;
            }

            Position other = (Position) obj;

            return other.X == X && other.Y == Y;
        }
    }

    class GridMap {
        private bool[,] Walls;
        private List<Position> Boxes;
        private Position Robot;

        public static GridMap Copy(GridMap other, bool doubleSize = false) {
            GridMap map = new GridMap();

            int factor = doubleSize ? 2 : 1;

            map.Walls = new bool[other.Walls.GetLength(0), other.Walls.GetLength(1)*factor];
            map.Boxes = new List<Position>();
            map.Robot = new Position(other.Robot.X*factor, other.Robot.Y);

            for (int y = 0 ; y < other.Walls.GetLength(0) ; y++) {
                for (int x = 0 ; x < other.Walls.GetLength(1) ; x++) {
                    map.Walls[y,x*factor] = other.Walls[y,x];
                    if (doubleSize) {
                        map.Walls[y, x*factor+1] = other.Walls[y,x];
                    }
                }
            }

            foreach (Position box in other.Boxes) {
                map.Boxes.Add(new Position(box.X*factor, box.Y));
            }

            return map;
        }

        public static GridMap ReadFromInput(string[] lines, int nLines) {
            GridMap map = new GridMap();

            int nColumns = lines[0].Length;

            map.Walls = new bool[nLines, nColumns];
            map.Boxes = new List<Position>();

            for (int y = 0 ; y < nLines ; y++) {
                var line = lines[y];
                for (int x = 0 ; x < line.Length ; x++) {
                    map.Walls[y,x] = line[x] == WALL_SYMBOL;
                    if (line[x] == BOX_SYMBOL) {
                        map.Boxes.Add(new Position(x, y));
                    }
                    else if (line[x] == ROBOT_SYMBOL) {
                        map.Robot = new Position(x, y);
                    }
                }
            }

            return map;
        }

        private bool HasWall(int x, int y) {
            if (x < 0 || y < 0 || x > Walls.GetLength(1) || y > Walls.GetLength(0)) {
                return true;
            }

            return Walls[y, x];
        }

        private bool HasWall(Position position) {
            return HasWall(position.X, position.Y);
        }

        private Position? GetWideBoxAt(int x, int y) {
            foreach(Position box in Boxes) {
                if (box.Y == y && (box.X == x || box.X+1 == x)) {
                    return box;
                }
            }

            return null;
        }

        bool CanMoveWideBox(Position position, Direction direction) {
            switch(direction) {
                case Direction.Up:
                    if (HasWall(position.X, position.Y-1) || HasWall(position.X+1, position.Y-1)) {
                        return false;
                    }
                    var boxUp = GetWideBoxAt(position.X, position.Y-1);
                    if (boxUp != null && !CanMoveWideBox(boxUp, direction)) {
                        return false;
                    }
                    boxUp = GetWideBoxAt(position.X+1, position.Y-1);
                    if (boxUp != null && !CanMoveWideBox(boxUp, direction)) {
                        return false;
                    }
                    return true;
                case Direction.Down:
                    if (HasWall(position.X, position.Y+1) || HasWall(position.X+1, position.Y+1)) {
                        return false;
                    }
                    var boxDown = GetWideBoxAt(position.X, position.Y+1);
                    if (boxDown != null && !CanMoveWideBox(boxDown, direction)) {
                        return false;
                    }
                    boxDown = GetWideBoxAt(position.X+1, position.Y+1);
                    if (boxDown != null && !CanMoveWideBox(boxDown, direction)) {
                        return false;
                    }
                    return true;
                case Direction.Left:
                    if (HasWall(position.X-1, position.Y)) {
                        return false;
                    }
                    var boxLeft = GetWideBoxAt(position.X-1, position.Y);
                    if (boxLeft != null && !CanMoveWideBox(boxLeft, direction)) {
                        return false;
                    }
                    return true;
                case Direction.Right:
                    if (HasWall(position.X+2, position.Y)) {
                        return false;
                    }
                    var boxRight = GetWideBoxAt(position.X+2, position.Y);
                    if (boxRight != null && !CanMoveWideBox(boxRight, direction)) {
                        return false;
                    }
                    return true;
            }
            return true;
        }

        bool CanMove(Position position, Direction direction) {
            Position nextPosition = new Position(position.X,position.Y);
            switch (direction) {
                case Direction.Up:
                    nextPosition.Y --;
                    break;
                case Direction.Down:
                    nextPosition.Y ++;
                    break;
                case Direction.Left:
                    nextPosition.X --;
                    break;
                case Direction.Right:
                    nextPosition.X ++;
                    break;
            }

            if (HasWall(nextPosition)) {
                return false;
            }

            foreach (Position box in Boxes) {
                if (box.Equals(nextPosition)) {
                    return CanMove(nextPosition, direction);
                }
            }

            return true;
        }

        private void MoveWideBox(Position box, Direction direction) {
            switch(direction) {
                case Direction.Up:
                    var boxUp = GetWideBoxAt(box.X, box.Y-1);
                    if (boxUp != null) {
                        MoveWideBox(boxUp, direction);
                    }
                    boxUp = GetWideBoxAt(box.X+1, box.Y-1);
                    if (boxUp != null) {
                        MoveWideBox(boxUp, direction);
                    }
                    box.Y --;
                    break;
                case Direction.Down:
                    var boxDown = GetWideBoxAt(box.X, box.Y+1);
                    if (boxDown != null) {
                        MoveWideBox(boxDown, direction);
                    }
                    boxDown = GetWideBoxAt(box.X+1, box.Y+1);
                    if (boxDown != null) {
                        MoveWideBox(boxDown, direction);
                    }
                    box.Y ++;
                    break;
                case Direction.Left:
                    var boxLeft = GetWideBoxAt(box.X-1, box.Y);
                    if (boxLeft != null) {
                        MoveWideBox(boxLeft, direction);
                    }
                    box.X --;
                    break;
                case Direction.Right:
                    var boxRight = GetWideBoxAt(box.X+2, box.Y);
                    if (boxRight != null) {
                        MoveWideBox(boxRight, direction);
                    }
                    box.X ++;
                    break;
            }
        }

        private void Move(Position position, Direction direction) {
            Position nextPosition = new Position(position.X,position.Y);
            switch (direction) {
                case Direction.Up:
                    nextPosition.Y --;
                    break;
                case Direction.Down:
                    nextPosition.Y ++;
                    break;
                case Direction.Left:
                    nextPosition.X --;
                    break;
                case Direction.Right:
                    nextPosition.X ++;
                    break;
            }

            foreach (Position box in Boxes) {
                if (box.Equals(nextPosition)) {
                    Move(box, direction);
                }
            }

            position.X = nextPosition.X;
            position.Y = nextPosition.Y;
        }

        public void RobotMove(Direction direction) {
            if (CanMove(Robot, direction)) {
                Move(Robot, direction);
            }
        }

        public void RobotMoveWide(Direction direction) {
            var nextPosition = new Position(Robot.X, Robot.Y);
            switch (direction) {
                case Direction.Up:
                    nextPosition.Y--;
                    break;
                case Direction.Down:
                    nextPosition.Y++;
                    break;
                case Direction.Left:
                    nextPosition.X--;
                    break;
                case Direction.Right:
                    nextPosition.X++;
                    break;
            }

            if (HasWall(nextPosition)) {
                return;
            }

            var box = GetWideBoxAt(nextPosition.X, nextPosition.Y);
            if (box != null) {
                if (!CanMoveWideBox(box, direction)) {
                    return;
                }

                MoveWideBox(box, direction);
            }

            Robot.X = nextPosition.X;
            Robot.Y = nextPosition.Y;
        }

        private bool HasWideBoxL(Position position) {
            foreach (Position box in Boxes) {
                if (box.Equals(position)) {
                    return true;
                }
            }
            return false;
        }

        private bool HasWideBoxR(Position position) {
            var lPos = new Position(position.X-1, position.Y);
            foreach (Position box in Boxes) {
                if (box.Equals(lPos)) {
                    return true;
                }
            }
            return false;
        }

        public void PrintMap(StreamWriter outputFile, bool wide = false) {
            for (int y = 0 ; y < Walls.GetLength(0) ; y++) {
                for (int x = 0 ; x < Walls.GetLength(1) ; x++) {
                    Position curPos = new Position(x,y);
                    if (HasWall(curPos)) {
                        outputFile.Write(WALL_SYMBOL);
                    }
                    else if (Robot.Equals(curPos)) {
                        outputFile.Write(ROBOT_SYMBOL);
                    }
                    else if (!wide && Boxes.Contains(curPos)) {
                        outputFile.Write(BOX_SYMBOL);
                    }
                    else if (wide && HasWideBoxL(curPos)) {
                        outputFile.Write(WBL_SYMBOL);
                    }
                    else if (wide && HasWideBoxR(curPos)) {
                        outputFile.Write(WBR_SYMBOL);
                    }
                    else {
                        outputFile.Write(EMPTY_SYMBOL);
                    }
                }
                outputFile.Write('\n');
            }
            outputFile.Write('\n');
        }

        public int GetGPS(Position position) {
            return position.Y*100 + position.X;
        }

        public int GetSumOfAllBoxesGPS() {
            int result = 0;
            foreach(Position box in Boxes) {
                result += GetGPS(box);
            }
            return result;
        }
    }

    GridMap Map;
    List<Direction> RobotInstructions;

    public Day15(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n');

        int splitIndex = 0;
        // parse grid until empty line
        while (splitIndex < lines.Length) {
            if (lines[splitIndex].Length == 0) {
                break;
            }
            splitIndex++;
        }

        Map = GridMap.ReadFromInput(lines, splitIndex);
        RobotInstructions = new List<Direction>();

        for (int i = splitIndex+1 ; i < lines.Length ; i ++) {
            var line = lines[i];
            for (int j = 0 ; j < line.Length ; j++) {
                switch (line[j]) {
                    case '^':
                        RobotInstructions.Add(Direction.Up);
                        break;
                    case 'v':
                        RobotInstructions.Add(Direction.Down);
                        break;
                    case '<':
                        RobotInstructions.Add(Direction.Left);
                        break;
                    case '>':
                        RobotInstructions.Add(Direction.Right);
                        break;
                }
            }
        }
    }

    public override string PartOne()
    {
        var map = GridMap.Copy(Map);
        foreach (Direction direction in RobotInstructions) {
            map.RobotMove(direction);
        }

        int result = map.GetSumOfAllBoxesGPS();
        return $"Sum of all boxes GPS after the robot finished moving : {result}";
    }

    public override string PartTwo()
    {
        var map = GridMap.Copy(Map, true);
        foreach (Direction direction in RobotInstructions) {
            map.RobotMoveWide(direction);
        }
        int result = map.GetSumOfAllBoxesGPS();
        return $"Sum of all boxes GPS in wide : {result}";
    }
}