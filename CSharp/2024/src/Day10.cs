class Day10 : Solver {
    struct Position {
        public int i;
        public int j;

        public Position(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }
    int[,] TopographicMap;

    public Day10(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        TopographicMap = new int[lines.Length, lines[0].Length];
        for (int i = 0 ; i < lines.Length ; i++) {
            for (int j = 0 ; j < lines[i].Length ; j++) {
                TopographicMap[i,j] = (int)char.GetNumericValue(lines[i][j]);
            }
        }
    
        GetAllTrailHeadsScores();
    }

    private bool IsOutOfMap(int i, int j) {
        return i < 0 || j < 0 || i >= TopographicMap.GetLength(0) || j >= TopographicMap.GetLength(1);
    }

    private void RecursiveSearchSummit(int i, int j, int previousValue, ref HashSet<Position> summits, ref int trails) {
        if (IsOutOfMap(i,j)) {
            return;
        }

        int value = TopographicMap[i,j];

        if (value != previousValue+1) {
            return;
        }

        if (value == 9) {
            summits.Add(new Position(i, j));
            trails ++;
            return;
        }

        RecursiveSearchSummit(i-1, j, value, ref summits, ref trails); // up
        RecursiveSearchSummit(i+1, j, value, ref summits, ref trails); // down
        RecursiveSearchSummit(i, j-1, value, ref summits, ref trails); // left
        RecursiveSearchSummit(i, j+1, value, ref summits, ref trails); // right
    }

    int PartOneResult;
    int PartTwoResult;

    private void GetAllTrailHeadsScores() {
        int result = 0;
        int trails = 0;
        for (int i = 0 ; i < TopographicMap.GetLength(0) ; i++) {
            for (int j = 0 ; j < TopographicMap.GetLength(1) ; j++) {
                if (TopographicMap[i,j] != 0) {
                    continue;
                }

                HashSet<Position> summits = new HashSet<Position>();
                RecursiveSearchSummit(i,j,-1, ref summits, ref trails);
                result += summits.Count;
            }
        }

        PartOneResult = result;
        PartTwoResult = trails;
    }

    public override string PartOne()
    {
        return $"Sum of all trailheads scores : {PartOneResult}";
    }

    public override string PartTwo()
    {
        return $"Sum of all trailheads scores with distinct trails : {PartTwoResult}";
    }
}