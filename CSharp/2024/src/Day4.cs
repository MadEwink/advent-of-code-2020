using System.Drawing;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Channels;
using System.Xml.XPath;

class Day4 : Solver {
    struct DirectionType {
        public int X {get;}
        public int Y {get;}

        public DirectionType(int x, int y) {
            X = x;
            Y = y;
        }
    }

    char[,] Grid;
    string WordToSearch = "XMAS";
    string CrossToSearch = "MAS";

    DirectionType[] PositiveDirections = {
        new DirectionType(0, 1), // down
        new DirectionType(1, 1), // down right
        new DirectionType(1, 0), // right
        new DirectionType(1, -1), // up right
    };

    DirectionType[] CrossPositiveDirections = {
        new DirectionType(1, 1), // down right
        new DirectionType(1, -1), // up right
    };

    public Day4(string inputPath) : base(inputPath) {
        ReadInput();
    }

    private void ReadInput() {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);

        // set grid dimensions
        int nLines = lines.Length;
        int nColumns = lines[0].Length; 
        Grid = new char[nLines, nColumns];

        // fill grid
        for (int i = 0 ; i < nLines ; i++) {
            for (int j = 0 ; j < nColumns ; j++) {
                Grid[i,j] = lines[i][j];
            }
        }
    }

    private bool CheckLetter(int posX, int posY, char letter) {
        if (posY < 0 || posX < 0 || posY >= Grid.GetLength(0) || posX >= Grid.GetLength(1)) {
            return false;
        }

        return Grid[posY,posX] == letter;
    }

    private int SearchWordAt(int posX, int posY) {
        if (Grid[posY, posX] != WordToSearch[0]) {
            return 0;
        }

        int result = 0;
        foreach (DirectionType dir in PositiveDirections) {
            // positive
            bool foundPositive = true;
            bool foundNegative = true;
            for (int index = 1 ; index < WordToSearch.Length ; index ++) {
                char letterAtIndex = WordToSearch[index];
                if (!CheckLetter(posX + index*dir.X, posY + index*dir.Y, letterAtIndex)) {
                    foundPositive = false;
                }
                if (!CheckLetter(posX - index*dir.X, posY - index*dir.Y, letterAtIndex)) {
                    foundNegative = false;
                }
                if (!foundPositive && !foundNegative) {
                    break;
                }
            }
            if (foundPositive) {
                result ++;
            }
            if (foundNegative) {
                result ++;
            }
        }
        return result;
    }

    private int SearchWordInGrid() {
        int result = 0;
        for (int i = 0 ; i < Grid.GetLength(0) ; i++) {
            for (int j = 0 ; j < Grid.GetLength(1) ; j++) {
                result += SearchWordAt(j, i);
            }
        }
        return result;
    }

    private bool SearchCrossArmAt(int posX, int posY, DirectionType positiveDir) {
        int middleIndex = CrossToSearch.Length/2;
        bool foundPositive = true;
        bool foundNegative = true;
        for (int i = 1; i <= middleIndex ; i ++) {
            // check middleIndex-i
            int index1 = middleIndex+i;
            int index2 = middleIndex-i;
            foundPositive = foundPositive && CheckLetter(posX+i*positiveDir.X, posY+i*positiveDir.Y, CrossToSearch[index1])
                && CheckLetter(posX-i*positiveDir.X, posY-i*positiveDir.Y, CrossToSearch[index2]);
            foundNegative = foundNegative && CheckLetter(posX-i*positiveDir.X, posY-i*positiveDir.Y, CrossToSearch[index1])
                && CheckLetter(posX+i*positiveDir.X, posY+i*positiveDir.Y, CrossToSearch[index2]);
        }

        return foundPositive || foundNegative;
    }

    private bool SearchCrossAt(int posX, int posY) {
        int middleIndex = CrossToSearch.Length/2;
        if (Grid[posY, posX] != CrossToSearch[middleIndex]) {
            return false;
        }

        // validate each arm individually
        foreach (DirectionType dir in CrossPositiveDirections) {
            if (!SearchCrossArmAt(posX, posY, dir)) {
                return false;
            }
        }

        return true;
    }

    private int SearchCrossInGrid() {
        if (CrossToSearch.Length % 2 != 1) {
            return 0;
        }
        
        int result = 0;
        for (int i = 0 ; i < Grid.GetLength(0) ; i++) {
            for (int j = 0 ; j < Grid.GetLength(1) ; j++) {
                if (SearchCrossAt(j, i)) {
                    result ++;
                }
            }
        }
        return result;
    }

    public override string PartOne()
    {
        int result = SearchWordInGrid();
        return $"Number of occurences of {WordToSearch} : {result}";
    }

    public override string PartTwo()
    {
        int result = SearchCrossInGrid();
        return $"Number of occurences of X-{CrossToSearch} : {result}";
    }
}