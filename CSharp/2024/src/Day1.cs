using System.Security.AccessControl;

class Day1 : Solver
{
    public Day1(string inputPath) : base(inputPath)
    {}

    struct LocationLists
    {
        public LocationLists(int size) {
            FirstList = new int[size];
            SecondList = new int[size];
        }

        public LocationLists(LocationLists other) {
            FirstList = (int[])other.FirstList.Clone();
            SecondList = (int[])other.SecondList.Clone();
        }

        public void SetListsAt(int index, int firstValue, int secondValue) {
            if (index < 0 || index > Length) {
                return;
            }
            FirstList[index] = firstValue;
            SecondList[index] = secondValue;
        }

        public void Sort() {
            Array.Sort(FirstList);
            Array.Sort(SecondList);
        }

        public int GetDifferenceSum() {
            int sum = 0;
            for (int i = 0 ; i < Length ; i++) {
                sum += Math.Abs(FirstList[i] - SecondList[i]);
            }
            return sum;
        }

        private int GetCountInSecondList(int number) {
            int occurences = 0;
            foreach (int id in SecondList) {
                if (id == number) {
                    occurences ++;
                }
            }
            return occurences;
        }

        public int GetSimilarityScore() {
            int similarityScore = 0;
            foreach (int id in FirstList) {
                similarityScore += id * GetCountInSecondList(id);
            }
            return similarityScore;
        }

        int[] FirstList;
        int[] SecondList;

        int Length {
            get {
                return FirstList.Length;
            }
        }
    }

    public override string PartOne()
    {
        LocationLists lists = GetListsFromInput(Input);
        LocationLists sortedLists = new LocationLists(lists);
        sortedLists.Sort();
        int result = sortedLists.GetDifferenceSum();

        return $"Sum of sorted lists difference : {result}";
    }

    public override string PartTwo()
    {
        LocationLists lists = GetListsFromInput(Input);
        int result = lists.GetSimilarityScore();
        return $"Similarity score : {result}";
    }

    LocationLists GetListsFromInput(string input)
    {
        string[] lines = input.Split('\n');
        LocationLists result = new LocationLists(lines.Length-1);

        for (int i = 0; i < lines.Length ; i++)
        {
            string line = lines[i];
            if (line.Length <= 0) {
                continue;
            }
            string[] splitLine = line.Split();
            result.SetListsAt(i, int.Parse(splitLine.First()), int.Parse(splitLine.Last()));
        }

        return result;
    }
}