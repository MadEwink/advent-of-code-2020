using System.Data.Common;

class Day3 : Solver {
    enum InstructionType {
        Multiply,
        Do,
        Dont,
    }

    struct InstructionStruct {
        public InstructionType type { get; }
        readonly int[]? parameters;

        public InstructionStruct(InstructionType type, int[]? parameters) {
            this.type = type;
            this.parameters = parameters;
        }
        
        public int Interprete() {
            switch(type) {
                case InstructionType.Multiply:
                    return Do_Multiply();
            }

            return 0;
        }

        private int Do_Multiply() {
            int result = 1;
            if (parameters == null) {
                return 0;
            }
            foreach (int parameter in parameters) {
                result *= parameter;
            }
            return result;
        }
    }

    List<InstructionStruct> Instructions;

    public Day3(string inputPath) : base(inputPath) {
        Instructions = new List<InstructionStruct>();
        ReadInput();
    }

    private void ReadInput() {
        int currentIndex = 0;
        while (currentIndex < Input.Length-4) {
            if (Input.Substring(currentIndex, 4) == "do()") {
                Instructions.Add(new InstructionStruct(InstructionType.Do, null));
                currentIndex ++;
                continue;
            }

            if (Input.Substring(currentIndex, 7) == "don't()") {
                Instructions.Add(new InstructionStruct(InstructionType.Dont, null));
                currentIndex ++;
                continue;
            }

            if (Input.Substring(currentIndex, 4) != "mul(") {
                currentIndex ++;
                continue;
            }

            // maximum string length looks like this : mul(XXX,YYY)
            int maxLength = Math.Min(12, Input.Length-currentIndex);

            // move past the instruction text
            currentIndex += 4;
            maxLength -= 4;

            int endIndex = Input.IndexOf(")", currentIndex, maxLength);

            // instruction is not valid because never closed
            if (endIndex < 0) {
                continue;
            }

            string parameters = Input.Substring(currentIndex, endIndex-currentIndex);
            string[] split = parameters.Split(',');

            // more than one comma in instruction means its invalid
            if (split.Length != 2) {
                continue;
            }

            if (ParseMultiplyInstruction(split[0], split[1])) {
                // set currentIndex past endIndex
                currentIndex = endIndex;
            }
        }
    }

    private bool ParseMultiplyInstruction(string firstParameter, string secondParameter) {
        if (!CheckMultiplyParameter(firstParameter) || !CheckMultiplyParameter(secondParameter)) {
            return false;
        }

        int[] parameters = new int[2];
        parameters[0] = int.Parse(firstParameter);
        parameters[1] = int.Parse(secondParameter);

        Instructions.Add(new InstructionStruct(InstructionType.Multiply, parameters));
        return true;
    }

    private bool CheckMultiplyParameter(string parameter) {
        if (parameter.Length == 0 || parameter.Length > 3) {
            return false;
        }

        foreach (char c in parameter) {
            if (!char.IsDigit(c)) {
                return false;
            }
        }

        return true;
    }

    public override string PartOne()
    {
        int result = 0;
        foreach (InstructionStruct instruction in Instructions) {
            if (instruction.type == InstructionType.Multiply) {
                result += instruction.Interprete();
            }
        }
        return $"Sum of all multiplications is : {result}";
    }

    public override string PartTwo()
    {
        int result = 0;
        bool enabled = true;
        foreach (InstructionStruct instruction in Instructions) {
            switch (instruction.type) {
                case InstructionType.Do:
                    enabled = true;
                    break;
                case InstructionType.Dont:
                    enabled = false;
                    break;
                case InstructionType.Multiply:
                    if (enabled) {
                        result += instruction.Interprete();
                    }
                    break;

            }
        }
        return $"Sum of all enabled multiplications is : {result}";
    }
}