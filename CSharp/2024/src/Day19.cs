using System.Data.Common;

class Day19 : Solver {
    List<string> AvailablePatterns;
    List<string> TargetPatterns;
    List<bool> PossiblePatterns;
    Dictionary<string, uint> Cache;

    private bool RecursiveConstructPattern(string curTowel, string targetPattern) {
        if (curTowel.Length == targetPattern.Length) {
            return targetPattern.StartsWith(curTowel);
        }

        if (curTowel.Length > 0 && !targetPattern.StartsWith(curTowel)) {
            return false;
        }

        foreach (string pattern in AvailablePatterns) {
            if ( RecursiveConstructPattern(curTowel + pattern, targetPattern) ) {
                return true;
            }
        }

        return false;
    }

    // Only check last pattern, since curTowel has been checked through previous iterations
    private bool CompareStrings(string curTowel, string lastPattern, string targetPattern) {
        for (int i = 0 ; i < lastPattern.Length ; i++) {
            int j = i+curTowel.Length;
            if (lastPattern[i] != targetPattern[j]) {
                return false;
            }
        }

        return true;
    }

    private bool HasInCache(string curTowel, string lastPattern, string targetPattern) {
        string key = targetPattern.Substring(curTowel.Length+lastPattern.Length);
        return Cache.ContainsKey(key);
    }

    private uint GetFromCache(string curTowel, string lastPattern, string targetPattern) {
        string key = targetPattern.Substring(curTowel.Length+lastPattern.Length);
        return Cache[key];
    }

    private void SetInCache(string curTowel, string lastPattern, string targetPattern, uint result) {
        string key = targetPattern.Substring(curTowel.Length+lastPattern.Length);
        Cache[key] = result;
    }

    private uint RecursiveConstructAllPattern(string curTowel, string lastPattern, string targetPattern) {
        int curLength = curTowel.Length + lastPattern.Length;
        if (curLength == targetPattern.Length) {
            return (uint)(CompareStrings(curTowel, lastPattern, targetPattern) ? 1 : 0);
        }

        if (curLength > targetPattern.Length) {
            return 0;
        }

        if (curLength > 0 && !CompareStrings(curTowel, lastPattern, targetPattern)) {
            return 0;
        }

        if (HasInCache(curTowel, lastPattern, targetPattern)) {
            return GetFromCache(curTowel, lastPattern, targetPattern);
        }


        uint sum = 0;
        foreach (string pattern in AvailablePatterns) {
            uint result = RecursiveConstructAllPattern(curTowel + lastPattern, pattern, targetPattern);
            sum += result;
        }

        SetInCache(curTowel, lastPattern, targetPattern, sum);

        return sum;
    }

    public Day19(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        AvailablePatterns = new();
        TargetPatterns = new();
        PossiblePatterns = new();
        Cache = new();

        foreach (string p in lines[0].Split(',', StringSplitOptions.TrimEntries)) {
            AvailablePatterns.Add(p);
        }

        for (int i = 1 ; i < lines.Length ; i++) {
            TargetPatterns.Add(lines[i]);
        }
    }

    public override string PartOne()
    {
        int result = 0;
        foreach (string targetPattern in TargetPatterns) {
            bool possible = RecursiveConstructPattern("", targetPattern);
            if (possible) {
                result ++;
            }
            PossiblePatterns.Add(possible);
        }
        return $"Number of possible designs : {result}";
    }

    public override string PartTwo()
    {
        uint result = 0;
        for (int i = 0 ; i < TargetPatterns.Count ; i++) {
            if (!PossiblePatterns[i]) {
                continue;
            }
            result += RecursiveConstructAllPattern("", "", TargetPatterns[i]);
        }
        return $"Sum of all ways to do different possible designs : {result}";
    }
}