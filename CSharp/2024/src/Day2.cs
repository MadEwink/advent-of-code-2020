using System.ComponentModel;
using System.Data;
using System.Data.Common;

class Day2 : Solver {
    Reports reports;

    public Day2(string inputPath) : base(inputPath) {
        reports = new Reports(Input);
    }

    struct Reports {
        List<int>[] values;

        public int Length {
            get {
                return values.Length;
            }
        }

        public Reports(string input) {
            string[] lines = input.Split('\n');
            values = new List<int>[lines.Length-1];
            for (int i = 0 ; i < Length ; i++) {
                string[] line = lines[i].Split();
                values[i] = new List<int>();
                for (int j = 0 ; j < line.Length ; j++) {
                    values[i].Add(int.Parse(line[j]));
                }
            }
        }

        bool IsDiffUnsafe(int reportIndex, int index1, int index2, bool ascending) {
            int diff = values[reportIndex][index1] - values[reportIndex][index2];
            return (ascending != (diff > 0) || diff == 0 || diff > 3 || diff < -3);
        }

        int EvaluateReportAt(int index, bool dampening = false, int startIndex = 0, bool ascending_value = false) {
            List<int> report = values[index];
            bool ascending = false;
            if (startIndex > 0) {
                ascending = ascending_value;
            } else {
                ascending = report[startIndex] < report[startIndex+1];
            }
            for (int i = startIndex+1 ; i < report.Count ; i++) {
                if (!IsDiffUnsafe(index, i, i-1, ascending)) {
                    continue;
                }

                if (dampening) {
                    // edge case on beginning, when removing a value can change ascending parameter
                    if (i <= 2) {
                        // try removing index 0
                        bool new_ascending = report[1] < report[2];
                        if (EvaluateReportAt(index, false, 1, new_ascending) < 0) {
                            return -1;
                        }
                        // try removing index 1
                        new_ascending = report[0] < report[2];
                        if (!IsDiffUnsafe(index, 2, 0, new_ascending) && EvaluateReportAt(index, false, 2, new_ascending) < 0) {
                            return -1;
                        }
                    }

                    // try by removing i-1
                    if ((i < 2 || !IsDiffUnsafe(index, i, i-2, ascending)) && EvaluateReportAt(index, false, i, ascending) < 0) {
                        return -1;
                    }

                    // try by removing i
                    if (i+1 >= report.Count || (!IsDiffUnsafe(index, i+1, i-1, ascending) && EvaluateReportAt(index, false, i+1, ascending) < 0)) {
                        return -1;
                    }
                }
                return i;
            }
            return -1;
        }

        public int CountSafeReports() {
            int result = 0;
            for (int i = 0 ; i < Length ; i++) {
                if (EvaluateReportAt(i) < 0) {
                    result ++;
                }
            }
            return result;
        }

        public int CountSafeReportsWithDampening() {
            int result = 0;
            for (int i = 0 ; i < Length ; i++) {
                int ret = EvaluateReportAt(i, true);
                if (ret < 0) {
                    result ++;
                }
            }
            return result;
        }

        public void PrintBackReports() {
            using (StreamWriter outputFile = new StreamWriter("day2-check.txt")) {
                for(int i = 0 ; i < Length ; i++) {
                    List<int> report = values[i];
                    foreach (int value in report) {
                        outputFile.Write($"{value} ");
                    }
                    outputFile.Write("\n");
                }
            }
        }
    }

    public override string PartOne()
    {
        int safeReportsCount = reports.CountSafeReports();
        return $"Number of safe reports : {safeReportsCount}";
    }

    public override string PartTwo()
    {
        int safeReportsCount = reports.CountSafeReportsWithDampening();
        return $"Number of safe reports with dampening : {safeReportsCount}";
    }
}