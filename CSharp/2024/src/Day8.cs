using System.ComponentModel;
using System.Data;

class Day8 : Solver {
    struct MapPosition {
        public int X;
        public int Y;

        public MapPosition(int x, int y) {
            X = x;
            Y = y;
        }
    }

    struct Antenna {
        public MapPosition Position;
        public char Frequency;

        public Antenna(int x, int y, char frequency) {
            Position = new MapPosition(x, y);
            Frequency = frequency;
        }
    }

    List<Antenna> Antennas;
    HashSet<MapPosition> Antinodes;
    int Lines;
    int Columns;

    public Day8(string inputPath) : base(inputPath) {
        Antennas = new List<Antenna>();
        Antinodes = new HashSet<MapPosition>();

        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        Lines = lines.Length;
        Columns = lines[0].Length;

        for (int y = 0 ; y < Lines ; y++) {
            for (int x = 0 ; x < Columns ; x++) {
                if (lines[y][x] == '.') {
                    continue;
                }

                Antennas.Add(new Antenna(x, y, lines[y][x]));
            }
        }
    }

    private bool IsInMap(MapPosition position) {
        return position.X >= 0 && position.Y >= 0 && position.X < Columns && position.Y < Lines;
    }

    private void AddAntinodesFor(Antenna antennaA, Antenna antennaB) {
        MapPosition firstAntinode = new MapPosition(2*antennaA.Position.X - antennaB.Position.X, 2*antennaA.Position.Y - antennaB.Position.Y);
        MapPosition secondAntinode = new MapPosition(2*antennaB.Position.X - antennaA.Position.X, 2*antennaB.Position.Y - antennaA.Position.Y);
        if (IsInMap(firstAntinode)) {
            Antinodes.Add(firstAntinode);
        }
        if (IsInMap(secondAntinode)) {
            Antinodes.Add(secondAntinode);
        }
    }

    private int CountAntinodesInMap() {
        for (int i = 0 ; i < Antennas.Count-1 ; i++) {
            for (int j = i+1 ; j < Antennas.Count ; j++) {
                if (Antennas[i].Frequency != Antennas[j].Frequency) {
                    continue;
                }
                AddAntinodesFor(Antennas[i], Antennas[j]);
            }
        }
        return Antinodes.Count;
    }

    public override string PartOne()
    {
        int result = CountAntinodesInMap();
        return $"Number of antinodes of same frequency antennas : {result}";
    }

    private void AddDistanceLessAntinodesFor(Antenna antennaA, Antenna antennaB) {
        int diffX = antennaA.Position.X - antennaB.Position.X;
        int diffY = antennaA.Position.Y - antennaB.Position.Y;
        // explore negative from antenna A
        int i = 0;
        while (true) {
            MapPosition antinode = new MapPosition(antennaA.Position.X - i*diffX, antennaA.Position.Y - i*diffY);
            if (!IsInMap(antinode)) {
                break;
            }

            Antinodes.Add(antinode);
            i++;
        }
        // explore positive antenna B
        i = 0;
        while (true) {
            MapPosition antinode = new MapPosition(antennaB.Position.X + i*diffX, antennaB.Position.Y + i*diffY);
            if (!IsInMap(antinode)) {
                break;
            }

            Antinodes.Add(antinode);
            i++;
        }
    }

    private int CountDistanceLessAntinodesInMap() {
        for (int i = 0 ; i < Antennas.Count-1 ; i++) {
            for (int j = i+1 ; j < Antennas.Count ; j++) {
                if (Antennas[i].Frequency != Antennas[j].Frequency) {
                    continue;
                }
                AddDistanceLessAntinodesFor(Antennas[i], Antennas[j]);
            }
        }
        return Antinodes.Count;
    }

    public override string PartTwo()
    {
        int result = CountDistanceLessAntinodesInMap();
        return $"Number of distanceless antinodes in map : {result}";
    }
}