using System.Data;

class Day6 : Solver {
    struct MapPosition {
        public int X;
        public int Y;
        
        public MapPosition(MapPosition other) {
            X = other.X;
            Y = other.Y;
        }
        
        public MapPosition(int x, int y) {
            X = x;
            Y = y;
        }

        public override readonly bool Equals(object? obj) {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            MapPosition pos = (MapPosition) obj;
            return pos.X == X && pos.Y == Y;
        }

        public override readonly int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    struct OrientedPosition {
        public MapPosition Position;
        public Direction CurrentDirection;

        public OrientedPosition(OrientedPosition other) {
            Position = new MapPosition(other.Position.X, other.Position.Y);
            CurrentDirection = other.CurrentDirection;
        }

        public OrientedPosition(int x, int y, Direction direction) {
            Position = new MapPosition(x, y);
            CurrentDirection = direction;
        }

        // override object.Equals
        public override readonly bool Equals(object? obj) {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            OrientedPosition pos = (OrientedPosition) obj;
            return pos.Position.X == Position.X && pos.Position.Y == Position.Y && pos.CurrentDirection == CurrentDirection;
        }

        public override readonly int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    enum Direction {
        Up,
        Right,
        Down,
        Left,
    }

    bool[,] ObstacleMap;
    OrientedPosition StartPosition;
    OrientedPosition GuardPosition;
    MapPosition CurrentTestedObstacle;

    HashSet<MapPosition> VisitedPositions;
    HashSet<OrientedPosition> OrientedGuardTrajectory;
    HashSet<MapPosition> InfiniteLoopObstaclesPositions;

    public Day6(string inputPath) : base(inputPath) {
        ReadInput();
        CurrentTestedObstacle = new MapPosition(-1, -1);
    }

    private void ReadInput() {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        int nColumns = lines[0].Length;

        ObstacleMap = new bool[lines.Length,nColumns];
        for (int i = 0 ; i < lines.Length ; i++) {
            for (int j = 0 ; j < lines[i].Length ; j++) {
                ObstacleMap[i,j] = lines[i][j] == '#';
                bool isStart = false;
                Direction startDirection = Direction.Up;
                switch (lines[i][j]) {
                    case '^':
                        startDirection = Direction.Up;
                        isStart = true;
                        break;
                    case '>':
                        startDirection = Direction.Right;
                        isStart = true;
                        break;
                    case 'V':
                        startDirection = Direction.Down;
                        isStart = true;
                        break;
                    case '<':
                        startDirection = Direction.Left;
                        isStart = true;
                        break;
                }
                if (isStart) {
                    StartPosition = new OrientedPosition(j, i, startDirection);
                }
            }
        }
    }

    private void PrintBack() {
        using (StreamWriter outputFile = new StreamWriter("temp/day6.txt")) {
            for (int i = 0 ; i < ObstacleMap.GetLength(0) ; i++) {
                for (int j = 0 ; j < ObstacleMap.GetLength(1) ; j++) {
                    if (ObstacleMap[i,j]) {
                        outputFile.Write('#');
                    } else if (StartPosition.Position.X == j && StartPosition.Position.Y == i) {
                        switch (StartPosition.CurrentDirection) {
                            case Direction.Up:
                                outputFile.Write('^');
                                break;
                            case Direction.Right:
                                outputFile.Write('>');
                                break;
                            case Direction.Down:
                                outputFile.Write('V');
                                break;
                            case Direction.Left:
                                outputFile.Write('<');
                                break;
                        }
                    } else {
                        outputFile.Write('.');
                    }
                }
                outputFile.Write('\n');
            }
        }
    }

    private bool IsOutOfMap(int x, int y) {
        return y < 0 || x < 0 || y >= ObstacleMap.GetLength(0) || x >= ObstacleMap.GetLength(1);
    }

    private bool GetObstacleInFrontOf(OrientedPosition position) {
        int i = position.Position.Y;
        int j = position.Position.X;
        switch (position.CurrentDirection) {
            case Direction.Up:
                i --;
                break;
            case Direction.Right:
                j ++;
                break;
            case Direction.Down:
                i ++;
                break;
            case Direction.Left:
                j --;
                break;
        }

        // added obstacle
        if (i == CurrentTestedObstacle.Y && j == CurrentTestedObstacle.X) {
            return true;
        }

        // outside of map is not considered obstacle
        if (IsOutOfMap(j, i)) {
            return false;
        }

        return ObstacleMap[i,j];
    }

    private Direction GetNextDirection(Direction direction) {
        switch (direction) {
            case Direction.Up:
                return Direction.Right;
            case Direction.Right:
                return Direction.Down;
            case Direction.Down:
                return Direction.Left;
            case Direction.Left:
                return Direction.Up;
            default:
                throw new ArgumentException();
        }
    }

    private void GuardStep() {
        if (GetObstacleInFrontOf(GuardPosition)) {
            GuardPosition.CurrentDirection = GetNextDirection(GuardPosition.CurrentDirection);
        } else {
            switch (GuardPosition.CurrentDirection) {
                case Direction.Up:
                    GuardPosition.Position.Y --;
                    break;
                case Direction.Right:
                    GuardPosition.Position.X ++;
                    break;
                case Direction.Down:
                    GuardPosition.Position.Y ++;
                    break;
                case Direction.Left:
                    GuardPosition.Position.X --;
                    break;
            }
        }
    }

    private int CountVisitedGuardPositions() {
        GuardPosition = new OrientedPosition(StartPosition);
        VisitedPositions = new HashSet<MapPosition>();
        OrientedGuardTrajectory = new HashSet<OrientedPosition>();
        while (!IsOutOfMap(GuardPosition.Position.X, GuardPosition.Position.Y)) {
            VisitedPositions.Add(new MapPosition(GuardPosition.Position));
            OrientedGuardTrajectory.Add(new OrientedPosition(GuardPosition));
            GuardStep();
        }
        return VisitedPositions.Count;
    }

    private bool IsInFrontOf(OrientedPosition position, int x, int y) {
        switch (position.CurrentDirection) {
            case Direction.Up:
                return x == position.Position.X && y == position.Position.Y - 1;
            case Direction.Right:
                return x == position.Position.X + 1 && y == position.Position.Y;
            case Direction.Down:
                return x == position.Position.X && y == position.Position.Y + 1;
            case Direction.Left:
                return x == position.Position.X - 1 && y == position.Position.Y;
            default:
                throw new ArgumentException();
        }
    }

    private bool TestInfiniteLoop() {
        GuardPosition = new OrientedPosition(StartPosition);
        HashSet<OrientedPosition> GuardOrientedPositions = new HashSet<OrientedPosition>();
        while (!IsOutOfMap(GuardPosition.Position.X, GuardPosition.Position.Y)) {
            if (!GuardOrientedPositions.Add(new OrientedPosition(GuardPosition))) {
                // position already visited with same orientation means infinite loop
                return true;
            }
            GuardStep();
        }
        return false;
    }

    private void TestPositionForInfiniteLoop(int x, int y) {
        // already an obstacle
        if (!IsOutOfMap(x, y) && ObstacleMap[y,x]) {
            return;
        }
        // in front of start position
        if (IsInFrontOf(StartPosition, x, y)) {
            return;
        }

        CurrentTestedObstacle.X = x;
        CurrentTestedObstacle.Y = y;
        if (TestInfiniteLoop()) {
            InfiniteLoopObstaclesPositions.Add(new MapPosition(x, y));
        }
    }

    private int CountNumberOfObstaclesCausingInfiniteLoop() {
        InfiniteLoopObstaclesPositions = new HashSet<MapPosition>();
        foreach (OrientedPosition pathPosition in OrientedGuardTrajectory) {
            // set obstacle in front of current path position and start from there
            GuardPosition = new OrientedPosition(pathPosition);
            switch(pathPosition.CurrentDirection) {
                case Direction.Up:
                    TestPositionForInfiniteLoop(pathPosition.Position.X, pathPosition.Position.Y-1);
                    break;
                case Direction.Right:
                    TestPositionForInfiniteLoop(pathPosition.Position.X+1, pathPosition.Position.Y);
                    break;
                case Direction.Down:
                    TestPositionForInfiniteLoop(pathPosition.Position.X, pathPosition.Position.Y+1);
                    break;
                case Direction.Left:
                    TestPositionForInfiniteLoop(pathPosition.Position.X-1, pathPosition.Position.Y);
                    break;
            }
        }
        return InfiniteLoopObstaclesPositions.Count;
    }

    public override string PartOne()
    {
        int result = CountVisitedGuardPositions();
        return $"Number of distinct positions visited by guard : {result}";
    }

    public override string PartTwo()
    {
        int result = CountNumberOfObstaclesCausingInfiniteLoop();
        return $"Number of single obstacles causing infinite loops : {result}";
    }
}