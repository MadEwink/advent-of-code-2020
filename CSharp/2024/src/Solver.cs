class Solver {
    protected string Input;

    public Solver(string inputPath)
    {
        using StreamReader reader = new(inputPath);
        Input = reader.ReadToEnd();
    }

    public virtual string PartOne() {
        return "Not computed";
    }
    public virtual string PartTwo() {
        return "Not computed";
    }
}