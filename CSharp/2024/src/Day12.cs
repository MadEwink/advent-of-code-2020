using System.Globalization;

class Day12 : Solver {
    class Plot {
        public char PlantType {get;}
        public int RegionId;

        public Plot(char type) {
            PlantType = type;
            RegionId = -1;
        }
    }

    class Region {
        public char PlantType;
        public int Area;
        public int Perimeter;
        public int Sides;

        public Region(char plantType) {
            PlantType = plantType;
            Area = 0;
            Perimeter = 0;
            Sides = 0;
        }
    }

    Plot[,] Plots;
    int RegionCount;
    List<Region> Regions;

    public Day12(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        Plots = new Plot[lines.Length, lines[0].Length];
        for (int i = 0 ; i < lines.Length ; i++) {
            for (int j = 0 ; j < lines[i].Length ; j++) {
                char plotType = lines[i][j];
                Plots[i,j] = new Plot(plotType);
            }
        }
        RegionCount = 0;
        Regions = new List<Region>();

        for (int i = 0 ; i < Plots.GetLength(0) ; i++) {
            for (int j = 0 ; j < Plots.GetLength(1) ; j++) {
                ParseRegion(i,j);
            }
        }

        CountHorizontalSides();
        CountVerticalSides();
    }

    private char GetPlotType(int i, int j) {
        if (i < 0 || j < 0 || i >= Plots.GetLength(0) || j >= Plots.GetLength(1)) {
            return ' ';
        }

        return Plots[i,j].PlantType;
    }

    private void RecursiveParseRegion(int i, int j, char plantType, int regionId) {
        if (GetPlotType(i,j) != plantType || Plots[i,j].RegionId >= 0) {
            return;
        }

        Plots[i,j].RegionId = regionId;

        int permimeterContrib = 0;
        if (GetPlotType(i-1, j) != plantType) {
            permimeterContrib++;
        }
        else {
            RecursiveParseRegion(i-1, j, plantType, regionId);
        }
        if (GetPlotType(i+1, j) != plantType) {
            permimeterContrib++;
        }
        else {
            RecursiveParseRegion(i+1, j, plantType, regionId);
        }
        if (GetPlotType(i, j-1) != plantType) {
            permimeterContrib++;
        }
        else {
            RecursiveParseRegion(i, j-1, plantType, regionId);
        }
        if (GetPlotType(i,j+1) != plantType) {
            permimeterContrib++;
        }
        else {
            RecursiveParseRegion(i, j+1, plantType, regionId);
        }

        Regions[regionId].Area ++;
        Regions[regionId].Perimeter += permimeterContrib;
    }

    private void ParseRegion(int i, int j) {
        if (Plots[i,j].RegionId >= 0) {
            return;
        }
        
        char plantType = Plots[i,j].PlantType;

        int regionId = RegionCount ++;
        Regions.Add(new Region(plantType));

        RecursiveParseRegion(i,j,plantType, regionId);
    }

    public override string PartOne()
    {
        int result = 0;
        for (int i = 0 ; i < RegionCount ; i++) {
            result += Regions[i].Area * Regions[i].Perimeter;
        }
        return $"Fencing price : {result}";
    }

    private void CountHorizontalSides() {
        for (int i = 0 ; i < Plots.GetLength(0) ; i++) {
            int curRegionId = -1;
            bool wasSameAbove = false;
            bool wasSameBelow = false;
            for (int j = 0 ; j < Plots.GetLength(1) ; j++) {
                Plot curPlot = Plots[i,j];
                // above
                if (GetPlotType(i-1, j) != curPlot.PlantType) {
                    if (wasSameAbove || curRegionId != curPlot.RegionId) {
                        Regions[curPlot.RegionId].Sides ++;
                    }
                    wasSameAbove = false;
                }
                else {
                    wasSameAbove = true;
                }
                // below
                if (GetPlotType(i+1, j) != curPlot.PlantType) {
                    if (wasSameBelow || curRegionId != curPlot.RegionId) {
                        Regions[curPlot.RegionId].Sides ++;
                    }
                    wasSameBelow = false;
                }
                else {
                    wasSameBelow = true;
                }
                // region id
                curRegionId = curPlot.RegionId;
            }
        }
    }

    private void CountVerticalSides() {
        for (int j = 0 ; j < Plots.GetLength(1) ; j++) {
            int curRegionId = -1;
            bool wasSameLeft = false;
            bool wasSameRight = false;
            for (int i = 0 ; i < Plots.GetLength(1) ; i++) {
                Plot curPlot = Plots[i,j];
                // left
                if (GetPlotType(i, j-1) != curPlot.PlantType) {
                    if (wasSameLeft || curRegionId != curPlot.RegionId) {
                        Regions[curPlot.RegionId].Sides ++;
                    }
                    wasSameLeft = false;
                }
                else {
                    wasSameLeft = true;
                }
                // below
                if (GetPlotType(i, j+1) != curPlot.PlantType) {
                    if (wasSameRight || curRegionId != curPlot.RegionId) {
                        Regions[curPlot.RegionId].Sides ++;
                    }
                    wasSameRight = false;
                }
                else {
                    wasSameRight = true;
                }
                // region id
                curRegionId = curPlot.RegionId;
            }
        }
    }

    public override string PartTwo()
    {
        int result = 0;
        for (int i = 0 ; i < RegionCount ; i++) {
            result += Regions[i].Area * Regions[i].Sides;
        }
        return $"Fencing price with bulk discount : {result}";
    }
}