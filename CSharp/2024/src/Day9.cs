using System.ComponentModel.DataAnnotations;

class Day9 : Solver {
    struct PartitionBlock {
        public int Id;
        public int Index;
        public int Size;

        public PartitionBlock(int id, int index, int size) {
            Id = id;
            Index = index;
            Size = size;
        }

        public bool IsEmpty() {
            return Id < 0;
        }
    }

    List<PartitionBlock> DiskMap;
    List<PartitionBlock> ReorderedDiskMap;
    List<PartitionBlock> WholeFileDiskMap;

    public Day9(string inputPath) : base(inputPath) {
        DiskMap = new List<PartitionBlock>();
        ReorderedDiskMap = new List<PartitionBlock>();
        WholeFileDiskMap = new List<PartitionBlock>();
        bool isEmpty = false;
        int curIndex = 0;
        int curId = 0;
        foreach( char c in Input) {
            if (char.IsWhiteSpace(c)) {
                break;
            }
            int size = (int)char.GetNumericValue(c);
            int index = isEmpty ? -1 : curId++;
            DiskMap.Add(new PartitionBlock(index, curIndex, size));
            curIndex += size;
            isEmpty = !isEmpty;
        }
    }

    private void PrintBack() {
        using (StreamWriter outputFile = new StreamWriter("temp/day9.txt")) {
            foreach( PartitionBlock block in DiskMap ) {
                outputFile.Write(block.Size);
            }
            outputFile.Write('\n');
        }
    }

    private long CalculateChecksum(List<PartitionBlock> disk) {
        long result = 0;
        foreach(PartitionBlock block in disk) {
            if (block.IsEmpty()) {
                continue;
            }

            for (int i = 0 ; i < block.Size ; i++) {
                int index = block.Index + i;
                result += index * block.Id;
            }
        }

        return result;
    }

    private void ReorderDiskMap() {
        int j = DiskMap.Count-1;
        int curUsedSize = 0;
        for (int i = 0 ; i < j ; i++) {
            PartitionBlock curBlock = DiskMap[i];
            if (!curBlock.IsEmpty()) {
                ReorderedDiskMap.Add(new PartitionBlock(curBlock.Id, curBlock.Index, curBlock.Size));
                continue;
            }

            int curMovedSize = 0;
            while (curMovedSize < curBlock.Size) {
                // get last file block
                while (j > i && DiskMap[j].IsEmpty()) {
                    j --;
                    curUsedSize = 0;
                }
                if (j <= i) {
                    break;
                }
                PartitionBlock blockToUse = DiskMap[j];
                // move as much as possible from block to use, that fits in empty block remaining size
                int sizeToMove = Math.Min(curBlock.Size - curMovedSize, blockToUse.Size - curUsedSize);
                ReorderedDiskMap.Add(new PartitionBlock(blockToUse.Id, curBlock.Index+curMovedSize, sizeToMove));
                // keep track of how much we moved into empty block
                curMovedSize += sizeToMove;
                // keep track of how much of the moved block we used
                curUsedSize += sizeToMove;
                // if we used all of it, prepare going to the next one
                if (curUsedSize >= blockToUse.Size) {
                    j--;
                    curUsedSize = 0;
                }
            }
        }
        
        PartitionBlock lastBlockUsed = DiskMap[j];
        if (!lastBlockUsed.IsEmpty() && curUsedSize < lastBlockUsed.Size) {
            PartitionBlock lastPlacedBlock = ReorderedDiskMap.Last();
            ReorderedDiskMap.Add(new PartitionBlock(lastBlockUsed.Id, lastPlacedBlock.Index+lastPlacedBlock.Size, lastBlockUsed.Size - curUsedSize));
        }
    }

    private void PrintDisk(List<PartitionBlock> disk) {
        using StreamWriter outputFile = new StreamWriter("temp/day9.txt");
        foreach( PartitionBlock block in disk ) {
            for (int i = 0 ; i < block.Size ; i++) {
                if (block.IsEmpty()) {
                    outputFile.Write('.');
                } else {
                    outputFile.Write(block.Id);
                }
            }
        }
    }

    private void CopyDisk(List<PartitionBlock> from, List<PartitionBlock> to) {
        foreach (PartitionBlock block in from) {
            to.Add(new PartitionBlock(block.Id, block.Index, block.Size));
        }
    }

    private void FuseWithNext(int index) {
        var block = WholeFileDiskMap[index];
        block.Size += WholeFileDiskMap[index+1].Size;
        WholeFileDiskMap.RemoveAt(index+1);
        WholeFileDiskMap[index] = block;
    }

    private void EmptyBlockAt(int index) {
        var block = WholeFileDiskMap[index];
        block.Id = -1;
        WholeFileDiskMap[index] = block;

        // fuse consecutive empty blocks

        int curIndex = index;
        if (index > 0 && WholeFileDiskMap[index-1].IsEmpty()) {
            curIndex --;
            FuseWithNext(curIndex);
        }

        if (curIndex+1 < WholeFileDiskMap.Count && WholeFileDiskMap[curIndex+1].IsEmpty()) {
            FuseWithNext(curIndex);
        }
    }

    private void MoveWholeFiles() {
        int i = WholeFileDiskMap.Count-1;
        while (i > 0) {
            var blockToMove = WholeFileDiskMap[i];
            if (blockToMove.IsEmpty()) {
                i--;
                continue;
            }

            for (int j = 0 ; j < i ; j++) {
                var destBlock = WholeFileDiskMap[j];
                if (!destBlock.IsEmpty() || destBlock.Size < blockToMove.Size) {
                    continue;
                }

                // destBlock is empty and can receive blockToMove
                EmptyBlockAt(i);
                blockToMove.Index = destBlock.Index;
                WholeFileDiskMap.Insert(j, blockToMove);
                if (blockToMove.Size == destBlock.Size) {
                    WholeFileDiskMap.RemoveAt(j+1);
                    // removed a block, so i must decrease twice
                    i--;
                }
                else {
                    destBlock.Index += blockToMove.Size;
                    destBlock.Size -= blockToMove.Size;
                    WholeFileDiskMap[j+1] = destBlock;
                }
                break;
            }

            // update i
            i--;
        }
    }

    public override string PartOne()
    {
        ReorderDiskMap();
        long result = CalculateChecksum(ReorderedDiskMap);
        return $"Compacted disk map checksum : {result}";
    }

    public override string PartTwo()
    {
        CopyDisk(DiskMap, WholeFileDiskMap);
        MoveWholeFiles();
        long result = CalculateChecksum(WholeFileDiskMap);
        return $"Whole file compacted disk map checksum : {result}";
    }
}