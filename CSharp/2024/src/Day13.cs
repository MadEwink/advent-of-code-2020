using System.ComponentModel.DataAnnotations;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

class Day13 : Solver {
    class ClawMachine {
        public int ButtonA_X;
        public int ButtonA_Y;
        public int ButtonB_X;
        public int ButtonB_Y;

        public long Prize_X;
        public long Prize_Y;

        public ClawMachine() {}

        public ClawMachine(ClawMachine other, long furtherPrize = 0) {
            ButtonA_X = other.ButtonA_X;
            ButtonA_X = other.ButtonA_X;
            ButtonB_Y = other.ButtonB_Y;
            ButtonB_Y = other.ButtonB_Y;
            Prize_X = other.Prize_X + furtherPrize;
            Prize_Y = other.Prize_Y + furtherPrize;
        }
    }

    List<ClawMachine> Machines;

    static int BUTTON_A_TOKENS = 3;
    static int BUTTON_B_TOKENS = 1;

    public Day13(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        Machines = new List<ClawMachine>();

        Regex regex = new Regex(@"\d+");

        for (int i = 0 ; i < lines.Length ; i ++) {
            if (lines[i].StartsWith("Button A:")) {
                var m = regex.Match(lines[i]);
                ClawMachine machine = new ClawMachine();
                machine.ButtonA_X = int.Parse(m.Value);
                m = m.NextMatch();
                machine.ButtonA_Y = int.Parse(m.Value);
                m = regex.Match(lines[i+1]);
                machine.ButtonB_X = int.Parse(m.Value);
                m = m.NextMatch();
                machine.ButtonB_Y = int.Parse(m.Value);
                m = regex.Match(lines[i+2]);
                machine.Prize_X = int.Parse(m.Value);
                m = m.NextMatch();
                machine.Prize_Y = int.Parse(m.Value);
                Machines.Add(machine);
                i+= 2;
            }
        }
        PrintMachines();
    }

    private void PrintMachines() {
        using StreamWriter streamWriter = new StreamWriter("temp/day13.txt");
        foreach (var machine in Machines) {
            streamWriter.WriteLine($"Button A: X+{machine.ButtonA_X}, Y+{machine.ButtonA_Y}");
            streamWriter.WriteLine($"Button B: X+{machine.ButtonB_X}, Y+{machine.ButtonB_Y}");
            streamWriter.WriteLine($"Prize: X={machine.Prize_X}, Y={machine.Prize_Y}");
        }
    }

    private static long GCD(long a, long b) {
        while (a != 0 && b != 0) {
            if (a > b)
                a %= b;
            else
                b %= a;
        }
        return a | b;
    }


    private long ComputeTokens(ClawMachine machine) {
        long gcd_a = GCD(machine.ButtonA_X, machine.ButtonA_Y);
        long factor_x = machine.ButtonA_Y / gcd_a;
        long factor_y = machine.ButtonA_X / gcd_a;
        long value1 = factor_x * machine.Prize_X - factor_y * machine.Prize_Y;
        long value2 = factor_x * machine.ButtonB_X - factor_y * machine.ButtonB_Y;
        if (value1 % value2 != 0) {
            return 0;
        }

        long b_presses = value1/value2;

        long value3 = machine.Prize_X - machine.ButtonB_X*b_presses;
        if (value3 % machine.ButtonA_X != 0) {
            return 0;
        }

        long a_presses = value3 / machine.ButtonA_X;

        return a_presses * BUTTON_A_TOKENS + b_presses * BUTTON_B_TOKENS;
    }

    public override string PartOne()
    {
        long result = 0;
        foreach (ClawMachine machine in Machines) {
            result += ComputeTokens(machine);
        }
        return $"Tokens used to reach max amount of prizes : {result}";
    }

    public override string PartTwo()
    {
        long result = 0;
        foreach (ClawMachine machine in Machines) {
            ClawMachine furtherMachine = new ClawMachine(machine, 10000000000000);
            result += ComputeTokens(furtherMachine);
        }
        return $"Tokens used to reach max amount of further prizes : {result}";
    }
}