using System.Data;

class Day5 : Solver {
    struct OrderingRule {
        public int X {get;}
        public int Y {get;}

        public OrderingRule(int x, int y) {
            X = x;
            Y = y;
        }
    }

    OrderingRule[] OrderingRules;
    List<int>[] Updates;

    public Day5(string inputPath) : base(inputPath) {
        ReadInput();
    }

    private void ReadInput() {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        int splitIndex = 0;
        while (splitIndex < lines.Length) {
            if (lines[splitIndex].Contains(',')) {
                break;
            }
            splitIndex++;
        }

        OrderingRules = new OrderingRule[splitIndex];
        Updates = new List<int>[lines.Length-splitIndex];

        for (int i = 0 ; i < splitIndex ; i++) {
            string[] lineSplit = lines[i].Split('|');
            // lineSplit should have two values
            OrderingRules[i] = new OrderingRule(int.Parse(lineSplit[0]), int.Parse(lineSplit[1]));
        }

        for (int i = 0 ; i < lines.Length-splitIndex ; i++) {
            string[] lineSplit = lines[i+splitIndex].Split(',');
            Updates[i] = new List<int>();
            for (int j = 0 ; j < lineSplit.Length ; j++) {
                Updates[i].Add(int.Parse(lineSplit[j]));
            }
        }
    }

    private void PrintBack() {
        using (StreamWriter outputFile = new StreamWriter("temp/day5.txt")) {
            foreach (OrderingRule rule in OrderingRules) {
                outputFile.WriteLine($"{rule.X}|{rule.Y}");
            }
            outputFile.Write('\n');
            foreach (var update in Updates) {
                outputFile.Write(update[0]);
                for (int i = 1 ; i < update.Count ; i++) {
                    outputFile.Write($",{update[i]}");
                }
                outputFile.Write('\n');
            }
        }
    }

    private bool TestUpdate(int index) {
        var update = Updates[index];
        for (int i = 0 ; i < update.Count ; i++) {
            int curValue = update[i];
            foreach (var rule in OrderingRules) {
                // check if there is a value after that should not be
                if (rule.Y != curValue) {
                    continue;
                }
                for (int j = i+1 ; j < update.Count ; j++) {
                    if (rule.X == update[j]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private int GetMiddleOfReordered(int index) {
        int i = 0;
        List<int> reorderedUpdate = new List<int>(Updates[index]);
        int count = reorderedUpdate.Count;
        while (i < count-1) {
            bool reordered = false;
            foreach (var rule in OrderingRules) {
                if (reordered) {
                    break;
                }

                if (rule.Y != reorderedUpdate[i]) {
                    continue;
                }

                for (int j = i+1 ; j < count ; j++) {
                    if (rule.X == reorderedUpdate[j]) {
                        // invert i and j and stop here
                        int temp = reorderedUpdate[i];
                        reorderedUpdate[i] = reorderedUpdate[j];
                        reorderedUpdate[j] = temp;
                        reordered = true;
                        break;
                    }
                }
            }

            if (!reordered) {
                i++;
            }
        }
        return reorderedUpdate[count/2];
    }

    public override string PartOne()
    {
        int result = 0;
        for (int i = 0 ; i < Updates.Length ; i++) {
            if (TestUpdate(i)) {
                result += Updates[i][Updates[i].Count/2];
            }
        }
        return $"Sum of middle of valid updates is : {result}";
    }

    public override string PartTwo()
    {
        int result = 0;
        for (int i = 0 ; i < Updates.Length ; i++) {
            if (!TestUpdate(i)) {
                result += GetMiddleOfReordered(i);
            }
        }
        return $"Sum of middle of reordered invalid updates is {result}";
    }
}