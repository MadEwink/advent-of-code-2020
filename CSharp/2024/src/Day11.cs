using System.Xml.XPath;

class Day11 : Solver {
    class EngravedStone {
        public long Value { get; private set; }

        public EngravedStone(long value) {
            Value = value;
        }

        public long Evolve() {
            if (Value == 0) {
                Value = 1;
                return -1;
            }

            int numberOfDigits = (int)Math.Ceiling(Math.Log10(Value+1));
            if (numberOfDigits % 2 == 0) {
                int powTen = (int) Math.Pow(10,numberOfDigits/2);
                long leftStoneValue = Value / powTen;
                long rightStoneValue = Value - (leftStoneValue*powTen);
                Value = leftStoneValue;
                return rightStoneValue;
            }

            Value *= 2024;
            return -1;
        }
    }

    class Cache {
        public long InitValue;
        public List<int> Steps;
        public List<int> Results;

        public Cache() {
            InitValue = 0;
            Steps = new List<int>();
            Results = new List<int>();
        }
    }

    List<EngravedStone> Stones;
    List<Cache> StoneCache;

    public Day11(string inputPath) : base(inputPath) {
        Stones = new List<EngravedStone>();
        foreach (string stone in Input.Split(' ', StringSplitOptions.RemoveEmptyEntries)) {
            Stones.Add(new EngravedStone(int.Parse(stone)));
        }
        StoneCache = new List<Cache>();
    }

    private void PrintStones() {
        using StreamWriter outputFile = new StreamWriter("temp/day11.txt");
        foreach (EngravedStone stone in Stones) {
            outputFile.Write(stone.Value);
            outputFile.Write(' ');
        }
        outputFile.Write('\n');
    }

    private void Blink(List<EngravedStone> stones) {
        for (int i = 0 ; i < stones.Count ; i++) {
            long ret = stones[i].Evolve();
            if (ret >= 0) {
                stones.Insert(i+1, new EngravedStone(ret));
                i++; // don't evolve the new stone
            }
        }
    }

    private void SetCache(long stoneValue, int steps, int result) {
        foreach (Cache cache in StoneCache) {
            if (cache.InitValue == stoneValue) {
                cache.Steps.Add(steps);
                cache.Results.Add(result);
                return;
            }
        }

        Cache newCache = new Cache();
        newCache.InitValue = stoneValue;
        newCache.Steps.Add(steps);
        newCache.Results.Add(result);
        StoneCache.Add(newCache);
    }

    private int GetCache(long stoneValue, int steps) {
        foreach (Cache cache in StoneCache) {
            if (cache.InitValue == stoneValue) {
                for (int i = 0 ; i < cache.Steps.Count ; i++) {
                    if (cache.Steps[i] == steps) {
                        return cache.Results[i];
                    }
                }
            }
        }

        return -1;
    }

    private int RecursiveBlink(List<EngravedStone> stones, int blinkCounter, int totalBlinks) {
        if (blinkCounter >= totalBlinks) {
            return 0;
        }

        int result = 0;
        if (stones.Count == 1) {
            long initValue = stones[0].Value;

            // if we can finish from cache, early return
            int fromCache = GetCache(initValue, totalBlinks-blinkCounter);
            if (fromCache >= 0) {
                result += fromCache;
                return result;
            }

            // compute 5 steps
            for (int i = 0 ; i < 5 ; i++) {
                Blink(stones);
            }
            blinkCounter += 5;
            result += stones.Count-1; 
            SetCache(initValue, 5, result);
        }

        foreach (EngravedStone stone in stones) {
            var stoneList = new List<EngravedStone> { stone };
            long initValue = stone.Value;
            int recResult = RecursiveBlink(stoneList, blinkCounter, totalBlinks);
            SetCache(initValue, totalBlinks - blinkCounter, recResult);
            result += recResult;
        }
        return result;
    }

    public override string PartOne()
    {
        int blinkTimes = 25;
        int result = Stones.Count + RecursiveBlink(Stones, 0, blinkTimes);
        return $"Number of stones after {blinkTimes} blinks : {result}";
    }

    public override string PartTwo()
    {
        int blinkTimes = 75;
        int result = Stones.Count + RecursiveBlink(Stones, 0, blinkTimes);
        return $"Number of stones after {blinkTimes} blinks : {result}";
    }
}