using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

class Day14 : Solver {
    class Robot {
        public int PositionX {get; private set;}
        public int PositionY {get; private set;}
        public int VelocityX {get; private set;}
        public int VelocityY {get; private set;}

        public Robot(int x, int y, int vx, int vy) {
            PositionX = x;
            PositionY = y;
            VelocityX = vx;
            VelocityY = vy;
        }

        public void Step(int width, int height) {
            PositionX += VelocityX;
            PositionY += VelocityY;

            while (PositionX < 0) {
                PositionX += width;
            }
            while (PositionY < 0) {
                PositionY += height;
            }

            PositionX %= width;
            PositionY %= height;
        }
    }

    List<Robot> Robots;

    public Day14(string inputPath) : base(inputPath) {
        Robots = new List<Robot>();

        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        var digitRegex = new Regex(@"-*\d+");
        foreach (string line in lines) {
            var m = digitRegex.Match(line);
            int px = int.Parse(m.Value); m = m.NextMatch();
            int py = int.Parse(m.Value); m = m.NextMatch();
            int vx = int.Parse(m.Value); m = m.NextMatch();
            int vy = int.Parse(m.Value);
            Robots.Add(new Robot(px, py, vx, vy));
        }
    }

    private List<Robot> CopyRobots() {
        var result = new List<Robot>();
        foreach (Robot robot in Robots) {
            result.Add(new Robot(robot.PositionX, robot.PositionY, robot.VelocityX, robot.VelocityY));
        }
        return result;
    }

    public override string PartOne()
    {
        int width = 101;
        int height = 103;

        var quadrant1 = 0;
        var quadrant2 = 0;
        var quadrant3 = 0;
        var quadrant4 = 0;

        List<Robot> robots = CopyRobots();

        foreach (Robot robot in robots) {
            for (int i = 0 ; i < 100 ; i++) {
                robot.Step(width, height);
            }
            if (robot.PositionX < width / 2 && robot.PositionY < height / 2) {
                quadrant1 ++;
            }
            else if (robot.PositionX > width / 2 && robot.PositionY < height / 2 ) {
                quadrant2 ++;
            }
            else if (robot.PositionX < width / 2 && robot.PositionY > height / 2 ) {
                quadrant3 ++;
            }
            else if (robot.PositionX > width / 2 && robot.PositionY > height / 2 ) {
                quadrant4 ++;
            }
        }
        
        int result = quadrant1*quadrant2*quadrant3*quadrant4;

        return $"Safety factor : {result}";
    }

    private void PrintPositions(List<Robot> robots, StreamWriter outputFile) {
        for (int y = 0 ; y < 103 ; y++) {
            for (int x = 0 ; x < 101 ; x++) {
                int countRobots = 0;
                foreach (Robot robot in robots) {
                    if (robot.PositionX == x && robot.PositionY == y) {
                        countRobots ++;
                    }
                }
                if (countRobots > 0) {
                    outputFile.Write(countRobots);
                }
                else {
                    outputFile.Write(' ');
                }
            }
            outputFile.Write('\n');
        }
    }

    private void CheckQuadrants(List<Robot> robots, int width, int height, StreamWriter outputFile) {
        var quadrant1 = 0;
        var quadrant2 = 0;
        var quadrant3 = 0;
        var quadrant4 = 0;

        foreach (Robot robot in robots) {
            if (robot.PositionX < width / 2 && robot.PositionY < height / 2) {
                quadrant1 ++;
            }
            else if (robot.PositionX > width / 2 && robot.PositionY < height / 2 ) {
                quadrant2 ++;
            }
            else if (robot.PositionX < width / 2 && robot.PositionY > height / 2 ) {
                quadrant3 ++;
            }
            else if (robot.PositionX > width / 2 && robot.PositionY > height / 2 ) {
                quadrant4 ++;
            }
        }

        outputFile.WriteLine($"{quadrant1*quadrant2*quadrant3*quadrant4},");
    }

    public override string PartTwo()
    {
        List<Robot> robots = CopyRobots();
        int width = 101;
        int height = 103;

        using StreamWriter outputFile = new StreamWriter("temp/day14-display.txt");

        for (int i = 0 ; i < 10000 ; i ++) {
            foreach(Robot robot in robots) {
                robot.Step(width, height);
            }
            // CheckQuadrants(robots, width, height, outputFile);
            // this point had a safety factor out of ordinary
            if (i == 6515) {
                PrintPositions(robots, outputFile);
            }
        }
        return $"Nothing to compute";
    }
}