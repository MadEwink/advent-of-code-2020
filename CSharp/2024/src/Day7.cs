using System.ComponentModel;

class Day7 : Solver {
    struct CalibrationEquation {
        public long Result;
        public List<long> Operands;
    }


    enum Operator {
        Add,
        Multiply,
        Concatenation,
    }

    CalibrationEquation[] Equations;
    public Day7(string inputPath) : base(inputPath) {
        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        Equations = new CalibrationEquation[lines.Length];
        for (int i = 0 ; i < lines.Length ; i++) {
            string[] split = lines[i].Split(':');
            Equations[i].Result = long.Parse(split[0]);
            Equations[i].Operands = new List<long>();
            foreach (string operand in split[1].Split(' ', StringSplitOptions.RemoveEmptyEntries)) {
                Equations[i].Operands.Add(long.Parse(operand));
            }
        }

        PrintBack();
    }

    private void PrintBack() {
        using (StreamWriter outputFile = new StreamWriter("temp/day7.txt")) {
            foreach (CalibrationEquation equation in Equations) {
                outputFile.Write($"{equation.Result}:");
                equation.Operands.ForEach(v => outputFile.Write($" {v}"));
                outputFile.Write('\n');
            }
        }
    }

    private bool RecursiveSearchResult(CalibrationEquation equation, int curIndex, long intermediateResult, Operator op, bool useConcatenation = false) {
        long result = intermediateResult;
        switch (op) {
            case Operator.Add:
                result += equation.Operands[curIndex];
                break;
            case Operator.Multiply:
                result *= equation.Operands[curIndex];
                break;
            case Operator.Concatenation:
                long log10 = (long)Math.Ceiling(Math.Log10(equation.Operands[curIndex]+1));
                result *= (long)Math.Pow(10, log10);
                result += equation.Operands[curIndex];
                break;
        }

        // last iteration
        if (curIndex >= equation.Operands.Count-1) {
            return result == equation.Result;
        }
        else {
            return RecursiveSearchResult(equation, curIndex+1, result, Operator.Add, useConcatenation)
                || RecursiveSearchResult(equation, curIndex+1, result, Operator.Multiply, useConcatenation)
                || useConcatenation && RecursiveSearchResult(equation, curIndex+1, result, Operator.Concatenation, useConcatenation);
        }
    }

    private bool SearchResult(CalibrationEquation equation, bool useConcatenation = false) {
        long first = equation.Operands[0];
        return RecursiveSearchResult(equation, 1, first, Operator.Add, useConcatenation)
            || RecursiveSearchResult(equation, 1, first, Operator.Multiply, useConcatenation)
            || useConcatenation && RecursiveSearchResult(equation, 1, first, Operator.Concatenation, useConcatenation);
    }

    public override string PartOne()
    {
        long result = 0;
        foreach (CalibrationEquation equation in Equations) {
            if (SearchResult(equation)) {
                result += equation.Result;
            }
        }
        return $"Sum of result of equations that can be computed : {result}";
    }

    public override string PartTwo()
    {
        long result = 0;
        foreach (CalibrationEquation equation in Equations) {
            if (SearchResult(equation, true)) {
                result += equation.Result;
            }
        }
        return $"Sum of result of equations that can be computed using concatenation : {result}";
    }
}