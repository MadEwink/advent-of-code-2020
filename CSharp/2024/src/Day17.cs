using System.Net.Http.Headers;
using System.Xml;

class Day17 : Solver {

    class Computer3B {
        public int RegisterA;
        public int RegisterB;
        public int RegisterC;

        public int InstructionPointer;

        readonly public List<int> Outputs;

        public Computer3B() {
            Outputs = new List<int>();
            InstructionPointer = 0;
        }

        public void Reset() {
            InstructionPointer = 0;
            RegisterA = 0;
            RegisterB = 0;
            RegisterC = 0;
            Outputs.Clear();
        }

        public void Output(int value) {
            Outputs.Add(value);
        }

        public string GetConcatenatedOutput() {
            string ret = "";
            foreach( int output in Outputs ) {
                ret += output;
                ret += ',';
            }
            ret = ret.Remove(ret.Length-1);
            return ret;
        }
    }

    class Instruction {
        public virtual void Operate(Computer3B c3b, int operand) {
            c3b.InstructionPointer += 2;
        }

        class InvalidValueException : Exception {}

        protected int GetComboOperandValue(Computer3B c3b, int operand) {
            switch(operand) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return operand;

                case 4:
                    return c3b.RegisterA;
                case 5:
                    return c3b.RegisterB;
                case 6:
                    return c3b.RegisterC;
                default:
                    throw new InvalidValueException();
            }
        }
    }

    class AdvInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            double numerator = c3b.RegisterA;
            double denominator = Math.Pow(2, GetComboOperandValue(c3b, operand));
            int result = (int) (numerator/denominator);
            c3b.RegisterA = result;
            base.Operate(c3b, operand);
        }
    }

    class BxlInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            c3b.RegisterB ^= operand;
            base.Operate(c3b, operand);
        }
    }

    class BstInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            c3b.RegisterB = GetComboOperandValue(c3b, operand) % 8;
            base.Operate(c3b, operand);
        }
    }
    
    class JnzInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            if (c3b.RegisterA != 0) {
                c3b.InstructionPointer = operand;
                return;
            }
            base.Operate(c3b, operand);
        }
    }

    class BxcInstruction : Instruction {
        public override void Operate(Computer3B c3b, int _)
        {
            c3b.RegisterB ^= c3b.RegisterC;
            base.Operate(c3b, _);
        }
    }

    class OutInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            int result = GetComboOperandValue(c3b, operand) % 8;
            c3b.Output(result);
            base.Operate(c3b, operand);
        }
    }

    class BdvInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            double numerator = c3b.RegisterA;
            double denominator = Math.Pow(2, GetComboOperandValue(c3b, operand));
            int result = (int) (numerator/denominator);
            c3b.RegisterB = result;
            base.Operate(c3b, operand);
        }
    }

    class CdvInstruction : Instruction {
        public override void Operate(Computer3B c3b, int operand)
        {
            double numerator = c3b.RegisterA;
            double denominator = Math.Pow(2, GetComboOperandValue(c3b, operand));
            int result = (int) (numerator/denominator);
            c3b.RegisterC = result;
            base.Operate(c3b, operand);
        }
    }

    readonly Computer3B C3b;
    readonly Instruction[] Instructions;
    readonly List<int> Program;

    public Day17(string inputPath) : base(inputPath) {
        Instructions = new Instruction[]{
            new AdvInstruction(),
            new BxlInstruction(),
            new BstInstruction(),
            new JnzInstruction(),
            new BxcInstruction(),
            new OutInstruction(),
            new BdvInstruction(),
            new CdvInstruction(),
        };

        Test();

        C3b = new Computer3B();
        Program = new List<int>();

        string[] lines = Input.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        C3b.RegisterA = int.Parse(lines[0].Split(':')[1]);
        C3b.RegisterB = int.Parse(lines[1].Split(':')[1]);
        C3b.RegisterC = int.Parse(lines[2].Split(':')[1]);

        string[] prog = lines[3].Split(':')[1].Split(',');
        foreach (string p in prog) {
            Program.Add( int.Parse(p) );
        }
    }

    private void ExecuteProgram(Computer3B c3b, List<int> program) {
        int failSafe = 0;
        while (c3b.InstructionPointer >= 0 && c3b.InstructionPointer < program.Count-1 && ++failSafe < 1000) {
            var instruction = Instructions[program[c3b.InstructionPointer]];
            var operand = program[c3b.InstructionPointer+1];
            instruction.Operate(c3b, operand);
        }
    }

    public override string PartOne()
    {
        ExecuteProgram(C3b, Program);
        return C3b.GetConcatenatedOutput();
    }

    private bool BeginningCorresponds(List<int> original, List<int> tested) {
        if (original == null || tested == null) {
            return false;
        }

        if (original.Count < tested.Count) {
            return false;
        }

        for (int i = 0 ; i < tested.Count ; i++) {
            if (original[i] != tested[i]) {
                return false;
            }
        }

        return true;
    }

    private bool ExecuteCopyProgram(Computer3B c3b, List<int> program) {
        int failSafe = 0;
        while (c3b.InstructionPointer >= 0 && c3b.InstructionPointer < program.Count-1 && ++failSafe < 1000) {
            var instruction = Instructions[program[c3b.InstructionPointer]];
            var operand = program[c3b.InstructionPointer+1];
            instruction.Operate(c3b, operand);
            if (!BeginningCorresponds(program, c3b.Outputs)) {
                return false;
            }
        }

        if (failSafe >= 1000) {
            return false;
        }

        // we know the beginning corresponds because otherwise we would have returned, so we only check size
        return program.Count == c3b.Outputs.Count;
    }

    private int BruteForcePart2() {
        for (int i = 0 ; i < 999999 ; i++) {
            C3b.Reset();
            C3b.RegisterA = i;
            if (ExecuteCopyProgram(C3b, Program)) {
                return i;
            }
        }

        return -1;
    }

    // Notes for part 2 :
    // Writing down my input, I see that there are operations on registers B and C, but A is only modified by an instruction dividing it by 8
    // This means that to obtain an output of 16 digits, its initial value must be over 8**16 and below 8**17
    // My program also only writes B, after a few operations, and I guess it should be possible to get some intel from these

    // For the record, here is my program :
    // 2,4 -- B = A % 8
    // 1,1 -- B = B ^ 1
    // 7,5 -- C = A / (2**B)
    // 1,5 -- B = B ^ 5
    // 4,3 -- B = B ^ C
    // 5,5 -- Output B
    // 0,3 -- A = A/8
    // 3,0 -- Loop if A != 0

    // When I output B, I have B = (((A % 8) ^ 1) ^ 5) ^ (A / 2**((A % 8) ^ 1) )
    // This is the same as ( (A % 8) ^ 4 ) ^ ( A / 2**((A % 8)^1) )
    // I have no idea how to use that though

    public override string PartTwo()
    {
        int result = BruteForcePart2();
        if (result < 0) {
            return $"Brute force failed, too bad";
        }

        return $"Lowest value for a self replicating program : {result}";
    }

    class TestErrorException : Exception {}

    private void Test() {

        Computer3B testComputer = new();
        List<int> program = new();

        testComputer.Reset();
        testComputer.RegisterC = 9;
        program = new List<int>() {2,6};
        ExecuteProgram(testComputer, program);
        if (testComputer.RegisterB != 1) {
            throw new TestErrorException();
        }

        testComputer.Reset();
        testComputer.RegisterA = 10;
        program = new List<int>() {5,0,5,1,5,4};
        ExecuteProgram(testComputer, program);
        if (testComputer.GetConcatenatedOutput() != "0,1,2") {
            throw new TestErrorException();
        }

        testComputer.Reset();
        testComputer.RegisterA = 2024;
        program = new List<int>() {0,1,5,4,3,0};
        ExecuteProgram(testComputer, program);
        if (testComputer.GetConcatenatedOutput() != "4,2,5,6,7,7,7,7,3,1,0" || testComputer.RegisterA != 0) {
            throw new TestErrorException();
        }

        testComputer.Reset();
        testComputer.RegisterB = 29;
        program = new List<int>() {1,7};
        ExecuteProgram(testComputer, program);
        if (testComputer.RegisterB != 26) {
            throw new TestErrorException();
        }

        testComputer.Reset();
        testComputer.RegisterB = 2024;
        testComputer.RegisterC = 43690;
        program = new List<int>() {4,0};
        ExecuteProgram(testComputer, program);
        if (testComputer.RegisterB != 44354) {
            throw new TestErrorException();
        }
    }
}