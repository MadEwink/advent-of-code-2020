﻿class Progam
{
    public static int Main(string[] args)
    {
        var solver = new Day19("inputs/day19.txt");
        Console.WriteLine(solver.PartOne());
        Console.WriteLine(solver.PartTwo());

        return 0;
    }
}