int countLines(const char*);
int readNumbersFromFile(int*, int, const char*);
int writeNumbersToFile(const int*, int, const char*);
int productOfSummingTwo(const int*, int);
int productOfSummingN(const int*, int, int);
