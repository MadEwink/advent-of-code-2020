#include <stdio.h>
#include "utils.h"

int main(int argc, char** argv) {
    if (argc <= 1) {
        fprintf(stderr, "Specify the input file : %s <input>\n", argv[0]);
        return -1;
    }

    const char* fileName = argv[1];

    int n = countLines(fileName);

    if (n <= 0) {
        fprintf(stderr, "Inexistant or empty file %s\n", argv[1]);
        return -1;
    }

    int numbers[n];

    readNumbersFromFile(numbers, n, fileName);

    writeNumbersToFile(numbers, n, "control.txt");

    int prodOfTwo = productOfSummingTwo(numbers, n);
    printf("%d\n", prodOfTwo);

    int test = productOfSummingN(numbers, n, 2);
    printf("%d\n", test);

    int prodOfThree = productOfSummingN(numbers, n, 3);
    printf("%d\n", prodOfThree);

    return 0;
}
