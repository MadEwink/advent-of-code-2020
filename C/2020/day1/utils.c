#include <stdio.h>

int countLines(const char* fileName) {
    FILE* f = fopen(fileName, "r");

    if (f == NULL) {
        return -1;
    }

    int n = 0;
    char c;

    do {
        c = fgetc(f);
        if (c == '\n') {
            n++;
        }
    } while (c != EOF);

    fclose(f);

    return n;
}

int readNumbersFromFile(int* numbers, int nLines, const char* fileName) {
    FILE* f = fopen(fileName, "r");

    if (f == NULL) {
        return -1;
    }

    for (int i = 0 ; i < nLines ; i++) {
        fscanf(f, "%d", numbers+i);
    }

    fclose(f);

    return 0;
}

int writeNumbersToFile(const int* numbers, int nLines, const char* fileName) {
    FILE* f = fopen(fileName, "w");

    if (f == NULL) {
        return -1;
    }

    for (int i = 0 ; i < nLines ; i++) {
        fprintf(f, "%d\n", numbers[i]);
    }

    fclose(f);
    
    return 0;
}

int productOfSummingTwo(const int* numbers, int nLines) {
    // search for the two elements of the list that sum to 2020
    for (int i = 0 ; i < nLines-1 ; i++) {
        for (int j = i+1 ; j < nLines ; j++) {
            if (numbers[i] + numbers[j] == 2020) {
               return numbers[i]*numbers[j];
            }
        }
    }
    return -1;
}

int recursiveProductOfSummingN(const int* numbers, int nLines, int recLevel, int startValue, int sumSoFar) {
    for (int i = startValue ; i < nLines-recLevel ; i++) {
        if (recLevel == 0) {
            if (sumSoFar + numbers[i] == 2020) {
                return numbers[i];
            }
        } else if (recLevel > 0) {
            int product = recursiveProductOfSummingN(numbers, nLines, recLevel-1, i+1, sumSoFar+numbers[i]);
            if (product != 0) {
                return product * numbers[i];
            }
        }
    }

    return 0;
}

int productOfSummingN(const int*numbers, int nLines, int n) {
    return recursiveProductOfSummingN(numbers, nLines, n-1, 0, 0);
}

int productOfSummingThree(const int* numbers, int nLines) {
    // search for the three elements of the list that sum to 2020
    for (int i = 0 ; i < nLines-2 ; i++) {
        for (int j = i+1 ; j < nLines-1 ; j++) {
            for (int k = j+1 ; k < nLines ; k++) {
                if (numbers[i] + numbers[j] + numbers[k] == 2020) {
                   return numbers[i]*numbers[j]*numbers[k];
                }
            }
        }
    }
    return -1;
}
