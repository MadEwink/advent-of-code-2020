# Advent of code 2020

This is my first attempt at doing the advent of code using the C language.

I already participated to the 2018 and 2019 editions, but in ADA.
After beginning the 2021 edition in ADA too, I realised I was not enjoying it at all, mostly because I'm not used to ADA enough.

Since it had been a long time I did not use C, I wanted to give it a try.

