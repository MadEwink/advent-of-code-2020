#include "password.h"

int satisfyPolicy(const char* password, int length, Policy policy) {
    int count = 0;
    for (int i = 0 ; i < length ; i++) {
        if (password[i] == '\0') {
            break;
        }
        if (password[i] == policy.letter) {
            count++;
            if (count > policy.maxLetterCount) {
                return 1;
            }
        }
    }

    if (count < policy.minLetterCount) {
        return -1;
    }

    return 0;
}

int matchPositionsPolicy(const char* password, int length, Policy policy) {
    int pos1 = policy.minLetterCount-1;
    int pos2 = policy.maxLetterCount-1;

    if (pos1 < 0 || pos1 >= length || pos2 < 0 || pos2 >= length) {
        return -1;
    }

    int matching = 0;

    if (password[pos1] == policy.letter) {
        matching++;
    }

    if (password[pos2] == policy.letter) {
        matching++;
    }

    return matching;
}

