#include <stdlib.h>
#include <stdio.h>
#include "password.h"
#include "utils.h"

int main(int argc, char** argv) {
    if (argc <= 1) {
        fprintf(stderr, "Specify the input file : %s <input>\n", argv[0]);
        return -1;
    }

    const char* fileName = argv[1];

    int n = countLines(fileName);

    if (n <= 0) {
        fprintf(stderr, "Inexistant or empty file %s\n", argv[1]);
        return -1;
    }

    Policy policies[n];
    char* passwords[n];
    for (int i = 0 ; i < n ; i++) {
        passwords[i] = malloc(sizeof(char*)*30);
    }

    readInput(policies, passwords, n, fileName);

    writeControl(policies, passwords, n, "control");

    int validFirst = 0;
    int validSecond = 0;
    for (int i = 0 ; i < n ; i++) {
        if (satisfyPolicy(passwords[i], 30, policies[i]) == 0) {
            validFirst ++;
        }
        if (matchPositionsPolicy(passwords[i], 30, policies[i]) == 1) {
            validSecond ++;
        }
    }

    printf("%d\n", validFirst);
    printf("%d\n", validSecond);

    for (int i = 0 ; i < n ; i++) {
        free(passwords[i]);
    }
}
