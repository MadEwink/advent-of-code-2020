typedef struct Policy {
    int minLetterCount;
    int maxLetterCount;
    char letter;
} Policy;

// returns 0 if the password satisfies the policy, -1 (resp 1) if the character has too few (resp too many) occurences
int satisfyPolicy(const char*, int, Policy);
// returns the number of matching positions, and -1 on error
int matchPositionsPolicy(const char*, int, Policy);
