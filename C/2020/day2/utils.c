#include <stdio.h>
#include "password.h"

int countLines(const char* fileName) {
    FILE* f = fopen(fileName, "r");

    if (f == NULL) {
        return -1;
    }

    int n = 0;
    char c;

    do {
        c = fgetc(f);
        if (c == '\n') {
            n++;
        }
    } while (c != EOF);

    fclose(f);

    return n;
}

int readInput(Policy* policies, char** passwords, int nLines, const char* fileName) {
    FILE* f = fopen(fileName, "r");

    if (f == NULL) {
        return -1;
    }

    for (int i = 0 ; i < nLines ; i++) {
        fscanf(f, "%d-%d %c: %[a-z]\n", &(policies[i].minLetterCount), &(policies[i].maxLetterCount), &(policies[i].letter), passwords[i]);
    }

    fclose(f);

    return 0;
}

int writeControl(const Policy* policies, const char** passwords, int nLines, const char* fileName) {
    FILE* f = fopen(fileName, "w");

    if (f == NULL) {
        return -1;
    }

    for (int i = 0 ; i < nLines ; i++) {
        fprintf(f, "%d-%d %c: %s\n", policies[i].minLetterCount, policies[i].maxLetterCount, policies[i].letter, passwords[i]);
    }

    fclose(f);

    return 0;
}
